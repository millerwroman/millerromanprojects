#ifndef LIB_H
#define LIB_H



#ifdef PHYSICS_UNITY_PLUGIN_EXPORT

#define PHYSICS_UNITY_PLUGIN_SYMBOL __declspec(dllexport)

#else



#ifdef PHYSICS_UNITY_PLUGIN_IMPORT

#define PHYSICS_UNITY_PLUGIN_SYMBOL __declspec(dllimport)

#else

#define PHYSICS_UNITY_PLUGIN_SYMBOL

#endif



#endif



#endif