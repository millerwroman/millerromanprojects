﻿using System;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SphereHull))]
public class SphereHullEditor : Editor
{
    // https://docs.unity3d.com/ScriptReference/Handles.ScaleSlider.html
    protected virtual void OnSceneGUI()
    {
        SphereHull selectedSphereHull = (SphereHull)target;

        if (selectedSphereHull == null)
        {
            return;
        }

        Handles.color = Color.green;

        EditorGUI.BeginChangeCheck();
        float scale = Handles.ScaleSlider(selectedSphereHull.radius, selectedSphereHull.center, Vector3.right, Quaternion.identity, selectedSphereHull.radius, 0.5f);
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(target, "Scale Radius");
            selectedSphereHull.radius = scale;
        }

        Handles.DrawWireDisc(selectedSphereHull.center, selectedSphereHull.transform.up, selectedSphereHull.radius);
        Handles.DrawWireDisc(selectedSphereHull.center, selectedSphereHull.transform.right, selectedSphereHull.radius);
        Handles.DrawWireDisc(selectedSphereHull.center, selectedSphereHull.transform.forward, selectedSphereHull.radius);
    }
}

[CustomEditor(typeof(AxisAlignedBoundingBoxHull))]
public class AxisAlignedBoundingBoxHullEditor : Editor
{
    // https://docs.unity3d.com/ScriptReference/Handles.ScaleSlider.html
    protected virtual void OnSceneGUI()
    {
        AxisAlignedBoundingBoxHull selectedBoxHull = (AxisAlignedBoundingBoxHull)target;

        if (selectedBoxHull == null)
        {
            return;
        }

        Handles.color = Color.green;

        EditorGUI.BeginChangeCheck();
        float scaleX = Handles.ScaleSlider(selectedBoxHull.Dimensions.x, selectedBoxHull.center, selectedBoxHull.transform.right, Quaternion.identity, selectedBoxHull.Dimensions.x * 0.5f, 0.5f);
        float scaleY = Handles.ScaleSlider(selectedBoxHull.Dimensions.y, selectedBoxHull.center, selectedBoxHull.transform.up, Quaternion.identity, selectedBoxHull.Dimensions.y * 0.5f, 0.5f);
        float scaleZ = Handles.ScaleSlider(selectedBoxHull.Dimensions.z, selectedBoxHull.center, selectedBoxHull.transform.forward, Quaternion.identity, selectedBoxHull.Dimensions.z * 0.5f, 0.5f);
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(target, "Scale Radius");
            selectedBoxHull.Dimensions = new Vector3(scaleX, scaleY, scaleZ);
        }

        Handles.DrawWireCube(selectedBoxHull.center, selectedBoxHull.Dimensions);
    }
}

[CustomEditor(typeof(ObjectBoundingBoxHull))]
public class ObjectBoundingBoxHullEditor : Editor
{
    // https://docs.unity3d.com/ScriptReference/Handles.ScaleSlider.html
    protected virtual void OnSceneGUI()
    {
        ObjectBoundingBoxHull selectedBoxHull = (ObjectBoundingBoxHull)target;

        if (selectedBoxHull == null)
        {
            return;
        }

        Handles.color = Color.green;

        EditorGUI.BeginChangeCheck();
        float scaleX = Handles.ScaleSlider(selectedBoxHull.Dimensions.x, selectedBoxHull.center, selectedBoxHull.transform.right, Quaternion.identity, selectedBoxHull.Dimensions.x * 0.5f, 0.5f);
        float scaleY = Handles.ScaleSlider(selectedBoxHull.Dimensions.y, selectedBoxHull.center, selectedBoxHull.transform.up, Quaternion.identity, selectedBoxHull.Dimensions.y * 0.5f, 0.5f);
        float scaleZ = Handles.ScaleSlider(selectedBoxHull.Dimensions.z, selectedBoxHull.center, selectedBoxHull.transform.forward, Quaternion.identity, selectedBoxHull.Dimensions.z * 0.5f, 0.5f);
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(target, "Scale Radius");
            selectedBoxHull.Dimensions = new Vector3(scaleX, scaleY, scaleZ);
        }

        Handles.matrix = selectedBoxHull.transform.localToWorldMatrix;
        Handles.DrawWireCube(selectedBoxHull.offset, selectedBoxHull.Dimensions);
    }
}