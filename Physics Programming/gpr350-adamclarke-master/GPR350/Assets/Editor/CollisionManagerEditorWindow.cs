﻿using System;
using UnityEngine;
using UnityEditor;



[ExecuteAlways]
public class CollisionManagerEditorWindow : EditorWindow
{
    private const string FILE_PATH = @"Assets\Resources\CollisionData.txt";
    private const string DEFAULT_LAYERS = "4~Default,UI,Water,Demo,-1111111111";


    // private Dictionary<string, Dictionary<string, bool>> layerDictionary = null;
    // private List<string> layerNames = null;

    private string[] layerNames = null;
    private bool[,] layerBools = null;
    private int numLayers = 0;

    private string addLayerString = "";


    [MenuItem(("Window/Yeet Collision Layers"))]
    public static void OpenWindow()
    {
        CollisionManagerEditorWindow window = (CollisionManagerEditorWindow)GetWindow(typeof(CollisionManagerEditorWindow));
        window.minSize = new Vector2(300, 300);
        window.Show();
    }

    private void OnEnable()
    {
        ReadData();
    }


    private void OnDisable()
    {
        SaveData();
    }

    private void OnGUI()
    {
        EditorGUILayout.LabelField("Collision Layer Manager", EditorStyles.boldLabel);
        DrawTriangle();
        DrawAddLayer();
        DrawResetData();
        DrawSaveData();
    }

    private void DrawSaveData()
    {
        EditorGUILayout.Space();

        EditorGUILayout.LabelField("Save Layers: ", EditorStyles.centeredGreyMiniLabel);
        if (GUILayout.Button("Save Changes"))
        {
            SaveData();
        }
    }

    private void DrawResetData()
    {
        EditorGUILayout.Space();

        EditorGUILayout.LabelField("Reset Data: ", EditorStyles.centeredGreyMiniLabel);
        if (GUILayout.Button("Reset to Default"))
        {
           ResetToDefault(); 
        }
    }

    private void ResetToDefault()
    {
        System.IO.File.WriteAllText(FILE_PATH, DEFAULT_LAYERS);
        ReadData();
    }

    private void Update()
    {
        
        if (Application.isPlaying)
        {
            SaveData();
        }
    }

    private void AddLayer()
    {
        numLayers++;
        Array.Resize(ref layerNames, layerNames.Length + 1);
        layerNames[numLayers] = addLayerString;
        AddNewArrayElement2D(ref layerBools, false);
    }

    private void AddNewArrayElement2D(ref bool[,] arr, bool newBool)
    {
        bool[,] temp = new bool[numLayers, numLayers];

        temp[0, 0] = false;
        for (int i = 0; i < numLayers - 1; i++)
        {
            for (int j = 1; j < numLayers; j++)
            {
                temp[i, j] = arr[i, j - 1];
            }
        }
        arr = temp;
    }

    private void DrawAddLayer()
    {
        EditorGUILayout.Space();

        EditorGUILayout.LabelField("Add Layers: ", EditorStyles.centeredGreyMiniLabel);
        addLayerString = EditorGUILayout.TextField(" New Layer Name:", addLayerString);
        if (GUILayout.Button("Add Layer"))
        {
            AddLayer();
            addLayerString = "";
        }
    }

    private void DrawTriangle()
    {


        GUILayout.BeginHorizontal();
        for (int i = 0; i <= numLayers; i++) //Horizontal Names
        {

            GUILayout.BeginVertical();
            GUILayout.Label(layerNames[i]);
            GUILayout.EndVertical();
        }
        GUILayout.EndHorizontal();



        GUILayout.BeginHorizontal();
        GUILayout.BeginVertical();

        for (int i = numLayers; i >= 1; i--) //Vertical Names
        {
            GUILayout.Label(layerNames[i]);
        }
        GUILayout.EndVertical();

        //Toggle Triangle
        for (int i = 0; i < numLayers; i++) //Horizontal 
        {
            GUILayout.BeginVertical();
            for (int j = 0; j < numLayers - i; j++) //Vertical
            {
                layerBools[i, j] = EditorGUILayout.Toggle(layerBools[i, j]);
            }
            GUILayout.EndVertical();
        }

        GUILayout.EndHorizontal();
    }

    private void SaveData()
    {

        string output = "";
        output += numLayers.ToString() + "~";
        for (int i = 1; i < numLayers + 1; i++)
        {
            output += layerNames[i] + ",";
        }

        output += "-";

        for (int i = 0; i < numLayers; i++)
        {
            for (int j = 0; j < numLayers - i; j++)
            {
                output += BoolToString(layerBools[j, i]);
            }
        }
        System.IO.File.WriteAllText(FILE_PATH, output);
    }

    private string BoolToString(bool b)
    {
        if (b) return "1";

        return "0";
    }

    private void ReadData()
    {
        AssetDatabase.Refresh();
        TextAsset textAsset = Resources.Load<TextAsset>("CollisionData");
        string text = textAsset.ToString();

        string temp = "";
        int i = 0;

        while (text[i] != '~')
        {
            temp += text[i];
            i++;
        }

        numLayers = int.Parse(temp);
        temp = "";
        layerNames = new string[numLayers + 1];
        layerNames[0] = "";
        layerBools = new bool[numLayers, numLayers];

        int placeX = 1;
        i++;
        while (text[i] != '-')
        {
            if (text[i] == ',')
            {
                layerNames[placeX] = temp;
                placeX++;
                temp = "";
            }
            else
            {
                temp += text[i];
            }
            i++;
        }

        i++;
        Vector2 place = Vector2.zero;
        for (int j = i; j < text.Length; j++)
        {

            layerBools[(int)place.x, (int)place.y] = Convert.ToBoolean(Convert.ToInt16(text[j].ToString()));

            if (place.x <= (numLayers - place.y) - 2)
            {
                place.x++;
            }
            else
            {
                place.x = 0;
                place.y++;
            }
        }

    }
}





