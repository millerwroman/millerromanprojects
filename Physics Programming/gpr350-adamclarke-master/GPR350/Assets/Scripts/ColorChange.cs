﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Collision = Yeet.Collision;

public class ColorChange : MonoBehaviour
{
    private Material material;

    void Start()
    {
        material = GetComponent<MeshRenderer>().material;
        // subscribe to all collision hulls on object
        foreach(CollisionHull hull in GetComponents<CollisionHull>())
        {
            hull.OnCollisionEvent += ChangeColor;
        }
    }

    private void FixedUpdate()
    {
        material.color = Color.red;
    }

    private void ChangeColor(object sender, Collision collision)
    {
        material.color = Color.green;
    }
}
