﻿using System.Runtime.InteropServices;

public class PhysicsUnityPlugin
{
    [DllImport("PhysicsUnityPlugin")]
    public static extern int InitFoo(int f_new = 0);

    [DllImport("PhysicsUnityPlugin")]
    public static extern int DoFoo(int bar = 0);

    [DllImport("PhysicsUnityPlugin")]
    public static extern int TermFoo();
}
