﻿using System;
using UnityEditor;
using UnityEngine;
using Collision = Yeet.Collision;
using UnityEngine.Events;

public class CollisionManager : MonoBehaviour
{
    public static string[] layerNames;
    public static bool[,] layerBools;
    public static int numLayers;
    public static UnityAction EVENT_READING;

    private void Start()
    {
        ReadData();
    }

    private void FixedUpdate()
    {
        // get all hulls in scene
        CollisionHull[] hulls = FindObjectsOfType<CollisionHull>();
        // loop through all hull pairs
        for (int i = 0; i < hulls.Length; i++)
        {
            // never test self or same pair twice
            // 0 - 1 2 3
            // 1 - 2 3
            // 2 - 3
            for (int j = i + 1; j < hulls.Length; j++)
            {
                if (!layerBools[hulls[i].layerPlace.x, hulls[j].layerPlace.y]) continue;

                Collision collison = null;


                if (CollisionHull.TestCollision(hulls[i], hulls[j], ref collison))
                {
                    // collisions w/ contact generation data
                    if (collison != null)
                    {
                        Vector3 prevPosA = collison.a.position;
                        Vector3 prevPosB = collison.b.position;

                        // resolve
                        collison.Resolve(Time.fixedDeltaTime);

                        // constraints
                        collison.a.ApplyConstraints(prevPosA, null);
                        collison.b.ApplyConstraints(prevPosB, null);
                        // update graphics because collision resolution manually changes pos & rot
                        collison.a.UpdateTransform();
                        collison.b.UpdateTransform();

                        // collision event w/ data
                        hulls[i].OnCollision(collison);
                        // swap so that rigidbody a is always this and b is other
                        collison.a = hulls[j].rigidbody;
                        collison.b = hulls[i].rigidbody;
                        hulls[j].OnCollision(collison);
                    }
                    // collisions w/o contact generation data
                    else
                    {
                        // collision event
                        hulls[i].OnCollision(null);
                        hulls[j].OnCollision(null);
                    }
                }
            }
        }
    }

    private void ReadData()
    {
        TextAsset textAsset = Resources.Load<TextAsset>("CollisionData");
        string text = textAsset.ToString();

        string temp = "";
        int i = 0;

        while (text[i] != '~')
        {
            temp += text[i];
            i++;
        }

        numLayers = int.Parse(temp);
        temp = "";
        layerNames = new string[numLayers];
        layerBools = new bool[numLayers, numLayers];

        int placeX = 0;
        i++;
        while (text[i] != '-')
        {
            if (text[i] == ',')
            {
                layerNames[placeX] = temp;
                placeX++;
                temp = "";
            }
            else
            {
                temp += text[i];
            }
            i++;
        }

        i++;
        Vector2 place = Vector2.zero;
        for (int j = i; j < text.Length; j++)
        {

            layerBools[(int)place.x, (int)place.y] = Convert.ToBoolean(Convert.ToInt16(text[j].ToString()));

            if (place.x <= (numLayers - place.y) - 2)
            {
                place.x++;
            }
            else
            {
                place.x = 0;
                place.y++;
            }
        }

        EVENT_READING();
    }
}
