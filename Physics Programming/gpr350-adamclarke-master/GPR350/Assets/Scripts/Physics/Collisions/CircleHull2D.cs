﻿using System;
using UnityEngine;

public class CircleHull2D : CollisionHull2D
{
    public CircleHull2D()
        : base(CollisionHullType2D.Circle)
    {

    }

    //  a point is a circle w/ radius 0
    [Range(0, 100)] public float radius;

    public override bool TestCollisionVsCircle(CircleHull2D _other, ref Collision2D collision2D)
    {
        // collision passes if distance between colliders is <= sum of radii
        // optimized collision if dist^2 <= sum radii^2
        // 1. get both centers
        // 2. take difference between centers
        // 3. distance squared = dot(diff, diff)
        // 4. sum of radii
        // 5. square sum of radii
        // 6. collision PASSES if: distSqr <= sumSqr
        // distance between two particles

        Vector2 diff = _other.center - center;
        float distSqrMagnitude = diff.sqrMagnitude;
        float sumRadii = radius + _other.radius;
        float sumRadiiSqr = sumRadii * sumRadii;
        float overlap = sumRadiiSqr - distSqrMagnitude;

        bool status = overlap >= 0;
        if (status)
        {
            // TODO: this doesn't work
            Vector2 contactPoint = center + diff * 0.5f;
            float distMagnitude = Mathf.Sqrt(distSqrMagnitude);
            Vector2 contactNormal = diff / distMagnitude;
            Vector2 relativeVel = particle.velocity - _other.particle.velocity;
            float separatingVel = Vector2.Dot(relativeVel, -contactNormal);
            // average
            float restitution = (coefficientOfRestitution + _other.coefficientOfRestitution) * 0.5f;
            Collision2D.Contact2D contact2D =
                new Collision2D.Contact2D(contactPoint, contactNormal, restitution, Mathf.Sqrt(overlap));
            collision2D = new Collision2D(particle, _other.particle, contact2D, separatingVel, true);
        }
        return status;
    }


    public override bool TestCollisionVsAABB(AxisAlignedBoundingBoxHull2D _other, ref Collision2D collision2D)
    {
        return TestCollisionVsAABB(_other, center, ref collision2D);
    }

    public bool TestCollisionVsAABB(AxisAlignedBoundingBoxHull2D _other, Vector2 _center, ref Collision2D collision2D)
    {
        // calculate closest point by clamping center to box extents; point vs circle test
        // collision passes if (distance) from (center of circle) to (center of circle clamped to AABB extents) is less than (radius)
        // 1. get circle center and AABB extents
        // 2. clamp center to extents of AABB on all axiis
        // 3. take difference between clamped point and center
        // 4. collision PASSES if: distance <= radius
        // 5. optimize: compare distance squared and radius squared

        _other.GetExtents(out Vector2 otherMinExtents, out Vector2 otherMaxExtents);
        Vector2 clampedCenter = new Vector2(
            Mathf.Clamp(_center.x, otherMinExtents.x, otherMaxExtents.x),
            Mathf.Clamp(_center.y, otherMinExtents.y, otherMaxExtents.y));
        Vector2 diff = clampedCenter - _center;
        float overlap = diff.sqrMagnitude - radius * radius;
        bool status = overlap <= 0;

        if (status)
        {
            Vector2 contactNormal =
                new Vector2(Sign(Mathf.Round(diff.normalized.x)), Sign(Mathf.Round(diff.normalized.y)));
            float restitution = (coefficientOfRestitution + _other.coefficientOfRestitution) * 0.5f;
            Vector2 relativeVel = particle.velocity - _other.particle.velocity;
            float separatingVelocity = Vector2.Dot(relativeVel, -contactNormal);

            Collision2D.Contact2D contact2D = new Collision2D.Contact2D(clampedCenter, contactNormal, restitution, overlap);
            collision2D = new Collision2D(particle, _other.particle, contact2D, separatingVelocity, status);
        }

        return status;
    }

    private int Sign(float f)
    {
        if (f > 0)
        {
            return 1;
        }
        if (f < 0)
        {
            return -1;
        }
        return 0;
    }

    public override bool TestCollisionVsOBB(ObjectBoundingBoxHull2D _other, ref Collision2D collision2D)
    {
        // same as above, but first...
        // move circle center into box's space by multiplying by its world transform inverse
        // 1. get circle center
        // 2. get AABB inverse world transform
        // 3. multiply circle center by inverse transform
        // 4. circle vs AABB

        Vector2 centerOBBSpace = (Vector2)_other.transform.InverseTransformPoint(center) + _other.center;
        return TestCollisionVsAABB(_other, centerOBBSpace, ref collision2D);
    }
}
