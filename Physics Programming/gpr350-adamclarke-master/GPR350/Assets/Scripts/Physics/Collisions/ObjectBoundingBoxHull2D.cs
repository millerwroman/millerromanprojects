﻿using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using Matrix4x4 = UnityEngine.Matrix4x4;
using Vector2 = UnityEngine.Vector2;

public class ObjectBoundingBoxHull2D : AxisAlignedBoundingBoxHull2D
{
    public ObjectBoundingBoxHull2D()
        : base(true)
    {
    }

    public new void GetExtents(out Vector2 minExtents, out Vector2 maxExtents)
    {
        // half dimensions to calculate corners
        Vector2 halfDim = dimensions / 2f;
        // rotation in radians
        float rotRad = Mathf.Deg2Rad * particle.rotation;

        // axis-aligned corners
        Vector2 bottomLeft = center - halfDim;
        Vector2 bottomRight = new Vector2(center.x + halfDim.x, center.y - halfDim.y);
        Vector2 topLeft = new Vector2(center.x - halfDim.x, center.y + halfDim.y);
        Vector2 topRight = center + halfDim;

        // rotate corners around center
        bottomLeft = Mafs.ZRotateAroundPivot(bottomLeft, center, rotRad);
        bottomRight = Mafs.ZRotateAroundPivot(bottomRight, center, rotRad);
        topLeft = Mafs.ZRotateAroundPivot(topLeft, center, rotRad);
        topRight = Mafs.ZRotateAroundPivot(topRight, center, rotRad);

        // min extents is least of lefts & bottoms
        minExtents.x = Mathf.Min(bottomLeft.x, topLeft.x);
        minExtents.y = Mathf.Min(bottomLeft.y, bottomRight.y);
        // max extents is max of rights & tops
        maxExtents.x = Mathf.Max(bottomRight.x, topRight.x);
        maxExtents.y = Mathf.Max(topLeft.y, topRight.y);

        //Debug.DrawLine(minExtents, maxExtents, Color.blue);
        //Debug.DrawLine(bottomRight, topLeft, Color.green);
        //Debug.DrawLine(bottomLeft, topRight, Color.green);
    }

    public override bool TestCollisionVsCircle(CircleHull2D _other, ref Collision2D collision2D)
    {
        // see circle
        return _other.TestCollisionVsOBB(this, ref collision2D);
    }

    public override bool TestCollisionVsAABB(AxisAlignedBoundingBoxHull2D _other, ref Collision2D collision2D)
    {
        // see aabb
        return _other.TestCollisionVsOBB(this, ref collision2D);
    }

    public override bool TestCollisionVsOBB(ObjectBoundingBoxHull2D _other, ref Collision2D collision2D)
    {
        // AABB-OBB part 2 twice
        // 1. ...

        return false;
    }
}
