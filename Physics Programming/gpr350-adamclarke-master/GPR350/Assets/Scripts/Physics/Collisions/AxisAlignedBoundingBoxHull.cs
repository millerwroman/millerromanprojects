﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Collision = Yeet.Collision;

public class AxisAlignedBoundingBoxHull : CollisionHull
{
    public AxisAlignedBoundingBoxHull()
        : base(CollisionHullType.AABB)
    {
    }

    public Vector3 Dimensions
    {
        get => dimensions;
        set
        {
            dimensions = value;
            halfDimensions = dimensions / 2f;
        }
    }
    [SerializeField]
    private Vector3 dimensions;
    private Vector3 halfDimensions;

    private void Awake()
    {
        Dimensions = dimensions;
    }


    public void GetExtents(out Vector3 minExtents, out Vector3 maxExtents)
    {
        minExtents = center - halfDimensions;
        maxExtents = center + halfDimensions;
    }

    public override bool TestCollisionVsSphere(SphereHull _other, ref Collision collision)
    {
        // call sphere's vs AABB
        return _other.TestCollisionVsAABB(this, ref collision);
    }

    public override bool TestCollisionVsAABB(AxisAlignedBoundingBoxHull _other, ref Collision collision)
    {
        // on each axis, test maxiumum extents exceed minimum extents of other
        // 1. get extents
        // 2. collision PASSES if maxA >= minB AND maxB >= minA on all axiis
        //
        //              maxA
        //     -----------
        //     |         |
        //     |         |
        //     |    A    |    maxB
        //     |       -------
        //     |       | |   |
        //     --------|--   |
        //    minA     |  B  |
        //             |     |
        //             -------
        //          minB

        GetExtents(out Vector3 aMinExtents, out Vector3 aMaxExtents);
        _other.GetExtents(out Vector3 bMinExtents, out Vector3 bMaxExtents);

        bool overlapX = aMaxExtents.x >= bMinExtents.x && bMaxExtents.x >= aMinExtents.x;
        bool overlapY = aMaxExtents.y >= bMinExtents.y && bMaxExtents.y >= aMinExtents.y;
        bool overlapZ = aMaxExtents.z >= bMinExtents.z && bMaxExtents.z >= aMinExtents.z;
        bool status = overlapX && overlapY && overlapZ;

        if (status)
        {
            //collision = new Collision(rigidbody, _other.rigidbody, new Collision.Contact(), 0, true);
            return true;
        }
        return false;
    }

    public override bool TestCollisionVsOBB(ObjectBoundingBoxHull _other, ref Collision collision)
    {
        // transform center into obb space
        Vector3 centerObbSpace = center;
        centerObbSpace -= _other.center;
        centerObbSpace = _other.rigidbody.worldTransformMatInv.MultiplyVector(centerObbSpace);
        centerObbSpace += _other.center;

        // aabb extents in obb space
        Vector3 aMinExtents = centerObbSpace - halfDimensions;
        Vector3 aMaxExtents = centerObbSpace + halfDimensions;

        // get obb extents in its local space
        Vector3[] obbLocalExtents = _other.GetAxisAlignedExtents(_other.center);
        Vector3 bMinExtents = obbLocalExtents[0];
        Vector3 bMaxExtents = obbLocalExtents[1];

        // aabb aabb
        bool overlapX = aMaxExtents.x >= bMinExtents.x && bMaxExtents.x >= aMinExtents.x;
        bool overlapY = aMaxExtents.y >= bMinExtents.y && bMaxExtents.y >= aMinExtents.y;
        bool overlapZ = aMaxExtents.z >= bMinExtents.z && bMaxExtents.z >= aMinExtents.z;
        bool status = overlapX && overlapY && overlapZ;

        if (status)
        {
            //collision = new Collision(rigidbody, _other.rigidbody, new Collision.Contact(), 0, true);
            return true;
        }
        return false;
    }
}
