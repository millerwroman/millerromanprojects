﻿using UnityEngine;

public class Collision2D
{
    public struct Contact2D
    {
        public Contact2D(Vector2 _point, Vector2 _normal, float _restitution, float _overlap)
        {
            point = _point;
            normal = _normal;
            restitution = _restitution;
            overlap = _overlap;
        }

        public Vector2 point;
        public Vector2 normal;
        public float restitution;
        public float overlap;
    }

    public Collision2D(Particle2D _a, Particle2D _b, Contact2D _contact2D, float _separatingVelocity, bool _status)
    {
        a = _a;
        b = _b;
        contact2D = _contact2D;
        separatingVelocity = _separatingVelocity;
        status = _status;
    }

    public Particle2D a = null;
    public Particle2D b = null;
    public Contact2D contact2D;
    public bool status;
    public float impulse = 0.0f;

    // any other pertinent information needed to resolve a collision.
    public float separatingVelocity;
}