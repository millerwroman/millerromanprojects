﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Collision = Yeet.Collision;
using Matrix4x4 = UnityEngine.Matrix4x4;

// https://github.com/gszauer/GamePhysicsCookbook

public struct MinMax
{
    public float min, max;
}

public class ObjectBoundingBoxHull : CollisionHull
{
    public ObjectBoundingBoxHull()
        : base(CollisionHullType.OBB)
    {
    }

    public Vector3 Dimensions
    {
        get => dimensions;
        set
        {
            dimensions = value;
            halfDimensions = dimensions / 2f;
        }
    }
    [SerializeField]
    private Vector3 dimensions;
    private Vector3 halfDimensions;

    // get all 8 vertices of OBB in world space
    public Vector3[] GetVertices(Vector3 hullCenter)
    {
        Vector3 halfDim = halfDimensions;
        // get rotation as matrix
        Matrix4x4 rotationMat = rigidbody.rotation;
        // face axiis
        Vector3 rotationMatCol0 = rotationMat.GetColumn(0);
        Vector3 rotationMatCol1 = rotationMat.GetColumn(1);
        Vector3 rotationMatCol2 = rotationMat.GetColumn(2);

        // extent = center + face axis * dimension
        return new Vector3[8]
        {
            hullCenter + rotationMatCol0 * halfDim.x + rotationMatCol1 * halfDim.y + rotationMatCol2 * halfDim.z,
            hullCenter - rotationMatCol0 * halfDim.x + rotationMatCol1 * halfDim.y + rotationMatCol2 * halfDim.z,
            hullCenter + rotationMatCol0 * halfDim.x - rotationMatCol1 * halfDim.y + rotationMatCol2 * halfDim.z,
            hullCenter + rotationMatCol0 * halfDim.x + rotationMatCol1 * halfDim.y - rotationMatCol2 * halfDim.z,
            hullCenter - rotationMatCol0 * halfDim.x - rotationMatCol1 * halfDim.y - rotationMatCol2 * halfDim.z,
            hullCenter + rotationMatCol0 * halfDim.x - rotationMatCol1 * halfDim.y - rotationMatCol2 * halfDim.z,
            hullCenter - rotationMatCol0 * halfDim.x + rotationMatCol1 * halfDim.y - rotationMatCol2 * halfDim.z,
            hullCenter - rotationMatCol0 * halfDim.x - rotationMatCol1 * halfDim.y + rotationMatCol2 * halfDim.z
        };
    }

    // {min, maxAAE} axis aligned extents
    public Vector3[] GetAxisAlignedExtents(Vector3 hullCenter)
    {
        Vector3[] axisAlignedExtents = new Vector3[2];
        axisAlignedExtents[0] = hullCenter - halfDimensions;
        axisAlignedExtents[1] = hullCenter + halfDimensions;
        return axisAlignedExtents;
    }

    // {min, max} min & max vertex values
    public Vector3[] GetObjectExtents(Vector3 hullCenter)
    {
        // vertices
        Vector3[] worldExtents = GetVertices(hullCenter);
        // min & max values of vertices
        Vector3[] objectExtents = new Vector3[2] { worldExtents[0], worldExtents[0] };
        for (int i = 1; i < worldExtents.Length; i++)
        {
            // min
            if (objectExtents[0].x > worldExtents[i].x)
                objectExtents[0].x = worldExtents[i].x;
            if (objectExtents[0].y > worldExtents[i].y)
                objectExtents[0].y = worldExtents[i].y;
            if (objectExtents[0].z > worldExtents[i].z)
                objectExtents[0].z = worldExtents[i].z;

            // max
            if (objectExtents[1].x < worldExtents[i].x)
                objectExtents[1].x = worldExtents[i].x;
            if (objectExtents[1].y < worldExtents[i].y)
                objectExtents[1].y = worldExtents[i].y;
            if (objectExtents[1].z < worldExtents[i].z)
                objectExtents[1].z = worldExtents[i].z;
        }
        return objectExtents;
    }

    private void Awake()
    {
        Dimensions = dimensions;
    }

    public override bool TestCollisionVsSphere(SphereHull _other, ref Collision collision)
    {
        return _other.TestCollisionVsOBB(this, ref collision);
    }

    public override bool TestCollisionVsAABB(AxisAlignedBoundingBoxHull _other, ref Collision collision)
    {
        return _other.TestCollisionVsOBB(this, ref collision);
    }

    // Separating Axis Theorem test OBB v OBB
    public override bool TestCollisionVsOBB(ObjectBoundingBoxHull _other, ref Collision collision)
    {
        // get rotations as matrices
        //Matrix4x4 aRotationMat = rigidbody.rotation;
        //Matrix4x4 bRotationMat = _other.rigidbody.rotation;

        // a in b space test
        Vector3 aCenterBSpace = center;
        aCenterBSpace -= _other.center;
        aCenterBSpace = _other.rigidbody.worldTransformMatInv.MultiplyVector(aCenterBSpace);
        aCenterBSpace += _other.center;

        Vector3[] aAxisAlignedExtentsBSpace = GetObjectExtents(aCenterBSpace);
        Vector3[] bAxisAlignedExtentsBSpace = _other.GetAxisAlignedExtents(_other.center);

        Vector3 aMinExtentsBSpace = aAxisAlignedExtentsBSpace[0];
        Vector3 aMaxExtentsBSpace = aAxisAlignedExtentsBSpace[1];
        Vector3 bMinExtentsBSpace = bAxisAlignedExtentsBSpace[0];
        Vector3 bMaxExtentsBSpace = bAxisAlignedExtentsBSpace[1];

        // aabb aabb
        bool overlapXBSpace = aMaxExtentsBSpace.x >= bMinExtentsBSpace.x && bMaxExtentsBSpace.x >= aMinExtentsBSpace.x;
        bool overlapYBSpace = aMaxExtentsBSpace.y >= bMinExtentsBSpace.y && bMaxExtentsBSpace.y >= aMinExtentsBSpace.y;
        bool overlapZBSpace = aMaxExtentsBSpace.z >= bMinExtentsBSpace.z && bMaxExtentsBSpace.z >= aMinExtentsBSpace.z;
        bool statusBSpace = overlapXBSpace && overlapYBSpace && overlapZBSpace;

        if (!statusBSpace)
        {
            return false;
        }

        // b in a space test
        Vector3 bCenterASpace = _other.center;
        bCenterASpace -= center;
        bCenterASpace = rigidbody.worldTransformMatInv.MultiplyVector(bCenterASpace);
        bCenterASpace += center;

        Vector3[] aAxisAlignedExtentsASpace = GetAxisAlignedExtents(center);
        Vector3[] bAxisAlignedExtentsASpace = _other.GetObjectExtents(bCenterASpace);

        Vector3 aMinExtentsASpace = aAxisAlignedExtentsASpace[0];
        Vector3 aMaxExtentsASpace = aAxisAlignedExtentsASpace[1];
        Vector3 bMinExtentsASpace = bAxisAlignedExtentsASpace[0];
        Vector3 bMaxExtentsASpace = bAxisAlignedExtentsASpace[1];

        // aabb aabb
        bool overlapXASpace = aMaxExtentsASpace.x >= bMinExtentsASpace.x && bMaxExtentsASpace.x >= aMinExtentsASpace.x;
        bool overlapYASpace = aMaxExtentsASpace.y >= bMinExtentsASpace.y && bMaxExtentsASpace.y >= aMinExtentsASpace.y;
        bool overlapZASpace = aMaxExtentsASpace.z >= bMinExtentsASpace.z && bMaxExtentsASpace.z >= aMinExtentsASpace.z;
        bool status2 = overlapXASpace && overlapYASpace && overlapZASpace;

        //var fooA = GetObjectExtents(center);
        //Debug.Log("A", gameObject);
        //Debug.DrawLine(fooA[0], fooA[1], Color.white);
        //var fooB = _other.GetObjectExtents(_other.center);
        //Debug.Log("B", _other.gameObject);
        //Debug.DrawLine(fooB[0], fooB[1], Color.blue);

        if (status2)
        {
            //collision = new Collision(rigidbody, _other.rigidbody, new Collision.Contact(), 0, true);
            return true;
        }

        return false;

        //// 15 axiis for separating axis theorem test
        //Vector3[] satAxiis = new Vector3[15];
        //// face normals of a
        //satAxiis[0] = new Vector3(aRotationMat.m00, aRotationMat.m01, aRotationMat.m02);
        //satAxiis[1] = new Vector3(aRotationMat.m10, aRotationMat.m11, aRotationMat.m12);
        //satAxiis[2] = new Vector3(aRotationMat.m20, aRotationMat.m21, aRotationMat.m22);
        //// face normals of b
        //satAxiis[3] = new Vector3(bRotationMat.m00, bRotationMat.m01, bRotationMat.m02);
        //satAxiis[4] = new Vector3(bRotationMat.m10, bRotationMat.m11, bRotationMat.m12);
        //satAxiis[5] = new Vector3(bRotationMat.m20, bRotationMat.m21, bRotationMat.m22);

        //// calculate 9 axiis
        //for (int i = 0; i < 3; ++i)
        //{
        //    satAxiis[6 + i * 3 + 0] = Vector3.Cross(satAxiis[i], satAxiis[0]);
        //    satAxiis[6 + i * 3 + 1] = Vector3.Cross(satAxiis[i], satAxiis[1]);
        //    satAxiis[6 + i * 3 + 2] = Vector3.Cross(satAxiis[i], satAxiis[2]);
        //}

        //// test passes if tests on every axis pass
        //bool status = true;
        //for (int i = 0; i < 15; ++i)
        //{
        //    // project extents of OBBs onto each of 15 axiis, get min and max projected extents
        //    MinMax aProjMinMax = ProjectMinMax(aExtents, satAxiis[i].normalized);
        //    MinMax bProjMinMax = ProjectMinMax(bExtents, satAxiis[i].normalized);

        //    // test overlap with min & max projections 
        //    bool overlap = aProjMinMax.max >= bProjMinMax.min && bProjMinMax.max >= aProjMinMax.min;
        //    // test failed, whole collision fails
        //    if (!overlap)
        //    {
        //        status = false;
        //        break;
        //    }
        //}

        //if (status)
        //{
        //    collision = new Collision(rigidbody, _other.rigidbody, new Collision.Contact(), 0, true);
        //    return true;
        //}

        //return false;
    }

    // get minimum and maxiumum of extents projected onto axis
    MinMax ProjectMinMax(Vector3[] obbExtents, Vector3 axis)
    {
        MinMax minMax;
        // project first extent onto axis for initial min & max values
        minMax.min = minMax.max = Vector3.Dot(obbExtents[0], axis);

        // project remaining extents onto axis to determine min & max
        for (int i = 1; i < 8; i++)
        {
            float proj = Vector3.Dot(obbExtents[i], axis);
            if (proj < minMax.min)
            {
                minMax.min = proj;
            }
            if (proj > minMax.max)
            {
                minMax.max = proj;
            }
        }

        return minMax;
    }
}
