﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Yeet;
using Collision = Yeet.Collision;
using Matrix4x4 = System.Numerics.Matrix4x4;

public class SphereHull : CollisionHull
{
    private SphereHull()
        : base(CollisionHullType.Sphere)
    {
    }

    public float radius = 0.5f;

    public override bool TestCollisionVsSphere(SphereHull _other, ref Collision collision)
    {
        // calc difference between centers
        // calc sum of radii
        // collision passes if length of diff b/w centers is less or equal to than sum of radii
        // optimize by compaing squared distances instead of taking square root
        Vector3 diff = center - _other.center;
        float sumRadii = radius + _other.radius;
        float difSqrMag = diff.sqrMagnitude;
        bool status = difSqrMag <= sumRadii * sumRadii;
        if (status)
        {
            float diffMag = Mathf.Sqrt(difSqrMag);
            // We manually create the normal, because we have the
            // size to hand.
            Vector3 normal = diff * (1.0f / diffMag);
            Vector3 contactNormal = normal;
            Vector3 contactPoint = rigidbody.position + diff * 0.5f;
            float penetration = (radius + _other.radius - diffMag);
            Contact contact = new Contact(contactPoint, contactNormal, penetration);
            collision = new Collision(rigidbody, _other.rigidbody, new List<Contact> { contact });

            return true;
        }
        return false;
    }

    public override bool TestCollisionVsAABB(AxisAlignedBoundingBoxHull _other, ref Collision collision)
    {
        // calculate closest point by clamping center to box extents; point vs sphere test
        // collision passes if (distance) from (center of sphere) to (center of sphere clamped to AABB extents) is less than (radius)
        // 1. get sphere center and AABB extents
        // 2. clamp center to extents of AABB on all axiis
        // 3. take difference between clamped point and center
        // 4. collision PASSES if: distance <= radius
        // 5. optimize: compare distance squared and radius squared

        // Transform the center of the sphere into box coordinates.
        //Vector3 centerBoxSpace = _other.transform.InverseTransformPoint(center);

        _other.GetExtents(out Vector3 otherMinExtents, out Vector3 otherMaxExtents);
        Vector3 clampedCenter = new Vector3(
            Mathf.Clamp(center.x, otherMinExtents.x, otherMaxExtents.x),
            Mathf.Clamp(center.y, otherMinExtents.y, otherMaxExtents.y),
            Mathf.Clamp(center.z, otherMinExtents.z, otherMaxExtents.z));
        Vector3 diff = clampedCenter - center;
        float diffSqrMag = diff.sqrMagnitude;
        float overlap = diffSqrMag - radius * radius;
        bool status = overlap <= 0;

        if (status)
        {
            Vector3 collisionNormal = (center - clampedCenter).normalized;
            float diffMag = Mathf.Sqrt(diffSqrMag);
            float penetration = radius - diffMag;
            Contact contact = new Contact(clampedCenter, collisionNormal, penetration);
            collision = new Collision(rigidbody, _other.rigidbody, new List<Contact> { contact });

            return true;
        }
        return false;
    }

    public override bool TestCollisionVsOBB(ObjectBoundingBoxHull _other, ref Collision collision)
    {
        // calculate closest point by clamping center in OBB space to box extents in OBB local space; point vs sphere test
        // collision passes if (distance) from (center of sphere) to (center of sphere clamped to AABB extents) is less than (radius)
        // 1. get sphere center and AABB extents
        // 2. clamp center to extents of AABB on all axiis
        // 3. take difference between clamped point and center
        // 4. collision PASSES if: distance <= radius
        // 5. optimize: compare distance squared and radius squared

        Vector3[] obbAxisAlignedExtents = _other.GetAxisAlignedExtents(_other.center);
        // pivot around obb center
        Vector3 sphereCenterObbSpace = center;
        sphereCenterObbSpace -= _other.center;
        sphereCenterObbSpace = _other.rigidbody.worldTransformMatInv.MultiplyVector(sphereCenterObbSpace);
        sphereCenterObbSpace += _other.center;

        // sphere AABB with sphere center in OBB space and OBB extents in OBB local space
        Vector3 clampedCenter = new Vector3(
            Mathf.Clamp(sphereCenterObbSpace.x, obbAxisAlignedExtents[0].x, obbAxisAlignedExtents[1].x),
            Mathf.Clamp(sphereCenterObbSpace.y, obbAxisAlignedExtents[0].y, obbAxisAlignedExtents[1].y),
            Mathf.Clamp(sphereCenterObbSpace.z, obbAxisAlignedExtents[0].z, obbAxisAlignedExtents[1].z));
        Vector3 diff = clampedCenter - sphereCenterObbSpace;
        float overlap = diff.sqrMagnitude - radius * radius;
        bool status = overlap <= 0;

        if (status)
        {
            //collision = new Collision(rigidbody, _other.rigidbody, new Collision.Contact(), 0, true);
            return true;
        }
        return false;
    }
}
