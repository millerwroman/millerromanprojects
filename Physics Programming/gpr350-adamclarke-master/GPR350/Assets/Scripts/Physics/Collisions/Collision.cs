﻿using System;
using System.Collections.Generic;
using System.Numerics;
using Vector3 = UnityEngine.Vector3;

namespace Yeet
{
    public class Contact
    {
        public Contact(Vector3 _point, Vector3 _normal, float _penetration)
        {
            point = _point;
            normal = _normal;
            penetration = _penetration;
        }

        public Vector3 point;
        public Vector3 normal;
        public float penetration;

        public void resolve(Rigidbody a, Rigidbody b, float restitution, float duration)
        {
            resolveVelocity(a, b, restitution, duration);
            resolveInterpenetration(a, b, duration);
        }

        public void resolveVelocity(Rigidbody a, Rigidbody b, float restitution, float duration)
        {
            // TODO
            // Find the velocity in the direction of the contact.
            Vector3 relativeVelocity = a.velocity - b.velocity;
            float separatingVelocity = Vector3.Dot(relativeVelocity, normal);

            // Check if it needs to be resolved.
            if (separatingVelocity > 0)
            {
                // The contact is either separating, or stationary;
                // no impulse is required.
                return;
            }

            // Calculate the new separating velocity.
            float newSepVelocity = -separatingVelocity * restitution;
            float deltaVelocity = newSepVelocity - separatingVelocity;

            // We apply the change in velocity to each object in proportion to
            // their inverse mass (i.e., those with lower inverse mass [higher
            // actual mass] get less change in velocity).
            float totalInverseMass = a.MassInv + b.MassInv;

            // If all particles have infinite mass, then impulses have no effect.
            if (totalInverseMass <= 0) return;

            // Calculate the impulse to apply.
            float impulse = deltaVelocity / totalInverseMass;

            // Find the amount of impulse per unit of inverse mass.
            Vector3 impulsePerIMass = normal * impulse;

            // Apply impulses: they are applied in the direction of the contact,
            // and are proportional to the inverse mass.
            a.AddImpulse(a.velocity + impulsePerIMass);

            // Particle 1 goes in the opposite direction
            b.AddImpulse(b.velocity + -impulsePerIMass);
        }

        public void resolveInterpenetration(Rigidbody a, Rigidbody b, float duration)
        {
            // TODO
            // If we don’t have any penetration, skip this step.
            if (penetration <= 0) return;

            // The movement of each object is based on their inverse mass,
            // so total that.
            float totalInverseMass = a.MassInv;
            if (b) totalInverseMass += b.MassInv;

            // If all particles have infinite mass, then we do nothing.
            if (totalInverseMass <= 0) return;

            // Find the amount of penetration resolution per unit
            // of inverse mass.
            Vector3 movePerIMass = normal * (penetration / totalInverseMass);

            // Calculate the movement amounts.
            Vector3 bMovement = movePerIMass * -b.MassInv;
            Vector3 aMovement = movePerIMass * a.MassInv;

            // Apply the penetration resolution.
            a.Move(aMovement);
            b.Move(bMovement);
        }
    }

    public class Collision
    {
        private List<Contact> contacts;
        public Rigidbody a, b;

        public Collision(Rigidbody _a, Rigidbody _b, List<Contact> _contacts)
        {
            a = _a;
            b = _b;
            contacts = _contacts;
        }

        public void Resolve(float duration)
        {
            foreach (var contact in contacts)
            {
                ResolveContact(contact, duration);
            }
        }

        private void ResolveContact(Contact contact, float duration)
        {
            float restitution = (a.restitution + b.restitution) * 0.5f;
            contact.resolve(a, b, restitution, duration);
        }
    }
}