﻿using UnityEngine;

public class AxisAlignedBoundingBoxHull2D : CollisionHull2D
{
    public AxisAlignedBoundingBoxHull2D()
    : base(CollisionHullType2D.AABB)
    {
    }

    public AxisAlignedBoundingBoxHull2D(bool isOBB)
    : base(isOBB ? CollisionHullType2D.OBB : CollisionHullType2D.AABB)
    {
    }

    public Vector2 dimensions;

    public virtual void GetExtents(out Vector2 minExtents, out Vector2 maxExtents)
    {
        Vector2 halfDim = dimensions * 0.5f;
        minExtents = center - halfDim;
        maxExtents = center + halfDim;
    }

    public override bool TestCollisionVsCircle(CircleHull2D _other, ref Collision2D collision2D)
    {
        // see circle
        return _other.TestCollisionVsAABB(this, ref collision2D);
    }

    public override bool TestCollisionVsAABB(AxisAlignedBoundingBoxHull2D _other, ref Collision2D collision2D)
    {
        // on each axis, maxiumum extent of a greater than minimum extent of b
        // 1. get extents
        // 2. collision PASSES if maxA >= minB AND maxB >= minA on all axiis
        //
        //              maxA
        //     -----------
        //     |         |
        //     |         |
        //     |    A    |    maxB
        //     |       -------
        //     |       | |   |
        //     --------|--   |
        //    minA     |  B  |
        //             |     |
        //             -------
        //          minB

        return AABBvsAABB(this, _other);
    }

    public override bool TestCollisionVsOBB(ObjectBoundingBoxHull2D _other, ref Collision2D collision2D)
    {
        // same as above twice:
        // first, find max extents of OBB, perform AABB vs this
        // then, transform this into OBB space, find max extents, AABB test again
        // 1. get AABB's extents and OBB's axis aligned extents
        // 2. test AABB's extents vs OBB's axis aligned extents
        // 3. entire test fails is above fails
        // 4. transform AABB into OBB space
        // 5. get now-rotated AABB's axis aligned extents
        // 6. test OBB axis aligned extents vs rotated AABB axis aligned extents

        GetExtents(out Vector2 minExtents, out Vector2 maxExtents);
        _other.GetExtents(out Vector2 otherMinExtents, out Vector2 otherMaxExtents);

        if (!AABBvsAABB(minExtents, maxExtents, otherMinExtents, otherMaxExtents))
        {
            return false;
        }


        // half dimensions to calculate corners
        Vector2 otherHalfDim = dimensions / 2f;
        // rotation in radians
        float otherRotRad = Mathf.Deg2Rad * _other.particle.rotation;

        // axis-aligned corners
        Vector2 otherBottomLeft = _other.center - otherHalfDim;
        Vector2 otherTopRight = _other.center + otherHalfDim;

        // rotate corners around center
        otherMinExtents = Mafs.ZRotateAroundPivot(otherBottomLeft, _other.center, otherRotRad);
        otherMaxExtents = Mafs.ZRotateAroundPivot(otherTopRight, _other.center, otherRotRad);

        // transform into OBB space
        //Vector2 obbSpaceCenter = _other.particle.transform.InverseTransformPoint(center);
        Vector2 obbSpaceCenter = center;

        // half dimensions to calculate corners
        Vector2 halfDim = dimensions / 2f;
        // rotation in radians
        float rotRad = Mathf.Deg2Rad * _other.particle.rotation;

        // axis-aligned corners
        Vector2 bottomLeft = obbSpaceCenter - halfDim;
        Vector2 topRight = obbSpaceCenter + halfDim;

        // rotate corners around center
        bottomLeft = Mafs.ZRotateAroundPivotInv(bottomLeft, obbSpaceCenter, rotRad);
        topRight = Mafs.ZRotateAroundPivotInv(topRight, obbSpaceCenter, rotRad);

        minExtents = bottomLeft;
        maxExtents = topRight;

        return AABBvsAABB(minExtents, maxExtents, otherMinExtents, otherMaxExtents);
    }

    private bool AABBvsAABB(AxisAlignedBoundingBoxHull2D a, AxisAlignedBoundingBoxHull2D b)
    {
        return false;

//        a.GetExtents(out Vector2 aMinExtents, out Vector2 aMaxExtents);
//        b.GetExtents(out Vector2 bMinExtents, out Vector2 bMaxExtents);
//
//        return AABBvsAABB(aMinExtents, aMaxExtents, bMinExtents, bMaxExtents);
    }

    private bool AABBvsAABB(Vector2 aMinExtents, Vector2 aMaxExtents, Vector2 bMinExtents, Vector2 bMaxExtents)
    {
        bool overlapX = aMaxExtents.x >= bMinExtents.x && bMaxExtents.x >= aMinExtents.x;
        bool overlapY = aMaxExtents.y >= bMinExtents.y && bMaxExtents.y >= aMinExtents.y;

        return overlapX && overlapY;
    }
}
