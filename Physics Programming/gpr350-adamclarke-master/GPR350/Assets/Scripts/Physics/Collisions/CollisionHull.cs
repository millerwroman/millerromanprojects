﻿using System;
using UnityEngine;
using Rigidbody = Yeet.Rigidbody;
using Collision = Yeet.Collision;

// lab 4 step 1: collision hulls
public abstract class CollisionHull : MonoBehaviour
{
    public enum CollisionHullType
    {
        Sphere,
        AABB,
        OBB
    }
    private CollisionHullType type { get; }

    public string layerName = "Default";
    [HideInInspector]
    public Vector2Int layerPlace = Vector2Int.zero;

    protected CollisionHull(CollisionHullType _type)
    {
        type = _type;
    }

    public new Rigidbody rigidbody { get; private set; }

    public Vector3 offset;
    public Vector3 center => transform.position + offset;

    //[Range(0, 1)]
    //public float coefficientOfRestitution;

    public event EventHandler<Collision> OnCollisionEvent;

    private void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        CollisionManager.EVENT_READING += SetLayer;
    }

    private void SetLayer()
    {
        for (int i = 0; i < CollisionManager.numLayers; i++)
        {
            if (layerName == CollisionManager.layerNames[i])
            {
                layerPlace = new Vector2Int(i,(CollisionManager.numLayers-1)-i);
                break;
            }
        }
    }

    public static bool TestCollision(CollisionHull a, CollisionHull b, ref Collision collision)
    {
        if (a.rigidbody == b.rigidbody) return false;

        bool status = false;
        switch (b.type)
        {
            case CollisionHullType.Sphere:
                status = a.TestCollisionVsSphere((SphereHull)b, ref collision);
                break;
            case CollisionHullType.AABB:
                status = a.TestCollisionVsAABB((AxisAlignedBoundingBoxHull)b, ref collision);
                break;
            case CollisionHullType.OBB:
                status = a.TestCollisionVsOBB((ObjectBoundingBoxHull)b, ref collision);
                break;
        }
        return status;
    }

    // lab 4 step 2
    public abstract bool TestCollisionVsSphere(SphereHull _other, ref Collision collision);
    public abstract bool TestCollisionVsAABB(AxisAlignedBoundingBoxHull _other, ref Collision collision);
    public abstract bool TestCollisionVsOBB(ObjectBoundingBoxHull _other, ref Collision collision);

    public void OnCollision(Collision collision)
    {
        OnCollisionEvent?.Invoke(this, collision);
    }
}
