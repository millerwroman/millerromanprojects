﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionManager2D : MonoBehaviour
{
    private void FixedUpdate()
    {
        // get all hulls in scene
        CollisionHull2D[] hulls = FindObjectsOfType<CollisionHull2D>();
        // loop through all hull pairs
        for (int i = 0; i < hulls.Length; i++)
        {
            // never test self or same pair twice
            // 0 - 1 2 3
            // 1 - 2 3
            // 2 - 3
            for (int j = i + 1; j < hulls.Length; j++)
            {
                Collision2D collison = null;
                if (CollisionHull2D.TestCollision(hulls[i], hulls[j], ref collison))
                {
                    ResolveCollision(ref collison);

                    hulls[i].OnCollision(collison);
                    collison.a = hulls[j].particle;
                    collison.b = hulls[i].particle;
                    hulls[j].OnCollision(collison);
                }
            }
        }
    }

    private void ResolveCollision(ref Collision2D collision2D)
    {
        if (collision2D.separatingVelocity > 0)
        {
            return;
        }

        float totalMass = collision2D.a.Mass + collision2D.b.Mass;
        if (totalMass <= 0)
        {
            return;
        }

        // apply restitution (bounciness) to separating velocity
        float newSeparatingVel = collision2D.separatingVelocity * collision2D.contact2D.restitution;
        float deltaVel = newSeparatingVel + collision2D.separatingVelocity;
        float impulse = deltaVel * totalMass;
        collision2D.impulse = impulse;
        Vector2 impulsePerMassInv = impulse * collision2D.contact2D.normal;
        collision2D.a.AddImpulse(impulsePerMassInv);
        collision2D.b.AddImpulse(impulsePerMassInv);

        if (collision2D.contact2D.overlap < 0)
        {
            return;
        }

        Vector3 move = collision2D.contact2D.normal * collision2D.contact2D.overlap;
        collision2D.a.Move(move);
        collision2D.b.Move(-move);
    }
}
