﻿using UnityEngine;
using Vector2 = UnityEngine.Vector2;

// labs 3 step 1: define particle inertias
static class MomentOfInteria
{
    public static float Disc(float mass, float radius)
    {
        return mass * radius * radius * 3.0f;
    }

    public static float Ring(float mass, float innerRadius, float outerRadius)
    {
        return mass * (outerRadius * outerRadius + innerRadius * innerRadius) * 0.5f;
    }

    public static float Rectangle(float mass, Vector2 dimensions)
    {
        return mass * (dimensions.x * dimensions.x + dimensions.y * dimensions.y * (1.0f / 12.0f));
    }

    public static float Rod(float mass, float length)
    {
        return mass * length * length * (1.0f / 12.0f);
    }


    /*3D Moment of Intertia Equations:
    SolidBox ===
    HollowBox ===
    SolidSphere ===
    HollowSphere ===
    SolidCylinder ===
    SolidCone ===
    */
    public static Matrix4x4 SolidBox(float mass, Vector3 dimensions)
    {
        float twelfthMass = 1f / 12f * mass;
        Vector3 dimensionsSqr = new Vector3(
            dimensions.x * dimensions.x,
            dimensions.y * dimensions.y,
            dimensions.z * dimensions.z);
        return new Matrix4x4(
            new Vector4(twelfthMass * (dimensionsSqr.y * dimensionsSqr.z), 0, 0, 0),
            new Vector4(0, twelfthMass * (dimensionsSqr.z * dimensionsSqr.x), 0, 0),
            new Vector4(0, 0, twelfthMass * (dimensionsSqr.x * dimensionsSqr.y), 0),
            new Vector4(0, 0, 0, 1));
    }

    public static Matrix4x4 HollowBox(float mass, Vector3 dimensions) //dimension order= width, height, depth
    {
        float fracMass = mass * (5f / 3f);
        Vector3 dimensionsSqr = new Vector3(
        dimensions.x * dimensions.x,
        dimensions.y * dimensions.y,
        dimensions.z * dimensions.z);

        return new Matrix4x4(
            new Vector4(fracMass * (dimensionsSqr.y + dimensionsSqr.z), 0, 0, 0),
            new Vector4(0, fracMass * (dimensionsSqr.z + dimensionsSqr.x), 0, 0),
            new Vector4(0, 0, fracMass * (dimensionsSqr.x + dimensionsSqr.y), 0),
            new Vector4(0, 0, 0, 1)); ;
    }

    public static Matrix4x4 SolidSphere(float mass, float radius)
    {
        float fracMass = mass * (2f / 5f);
        float radiusSqr = radius * radius;

        return new Matrix4x4(
            new Vector4(fracMass * radiusSqr, 0, 0, 0),
            new Vector4(0, fracMass * radiusSqr, 0, 0),
            new Vector4(0, 0, fracMass * radiusSqr, 0),
            new Vector4(0, 0, 0, 1));
    }

    public static Matrix4x4 HollowSphere(float mass, float radius)
    {
        float fracMass = mass * (2f / 3f);
        float radiusSqr = radius * radius;

        return new Matrix4x4(
        new Vector4(fracMass * radiusSqr, 0, 0, 0),
        new Vector4(0, fracMass * radiusSqr, 0, 0),
        new Vector4(0, 0, fracMass * radiusSqr, 0),
        new Vector4(0, 0, 0, 1));
    }

    public static Matrix4x4 SolidCylinder(float mass, float radius, float height)
    {
        float twelfthMass = 1f / 12f * mass;
        float radiusSqr = radius * radius;
        float heightSqr = height * height;

        return new Matrix4x4(
             new Vector4(twelfthMass * (3 * radius + heightSqr), 0, 0, 0),
             new Vector4(0, twelfthMass * (3 * radius + heightSqr),0,0),
             new Vector4(0,0,(mass*0.5f)*radiusSqr,0),
             new Vector4(0, 0, 0, 1));
    }

    public static Matrix4x4 SolidCone(float mass, float radius, float height)
    {
        float threeFithM = mass * (2f / 5f);
        float threeTwentyM = mass * (3 / 30);
        float radisuSqr = radius * radius;
        float heightSqr = height * height;

        return new Matrix4x4(
            new Vector4((threeFithM*heightSqr) +(threeTwentyM*radisuSqr),0,0,0 ),
            new Vector4(0, (threeFithM * heightSqr) + (threeTwentyM * radisuSqr),0,0),
            new Vector4(0,0,mass*0.3f*radisuSqr,0),
            new Vector4(0,0,0,1));
    }
}
