﻿using System;
using UnityEngine;

// lab 2 step 3: force generators
public static class ForceGenerator
{
    // Fg = mg
    public static Vector3 Gravity(float particleMass, float gravConstant, Vector3 worldUp)
    {
        Vector3 forceGravity = particleMass * gravConstant * worldUp;
        return forceGravity;
    }

    // Fn = proj(f_gravity, surfaceNormal_unit)
    public static Vector3 Normal(Vector3 forceGravity, Vector3 surfaceNormal)
    {
        // projection vector of -force gravity onto surface normal
        //     essentially Fg*cos(angle) for more dimensions
        var projGravOnNormal = Vector3.Dot(surfaceNormal, -forceGravity) /
                    surfaceNormal.sqrMagnitude *
                    surfaceNormal;
        // normal force in direction of surface normal at magnitude of projection
        return surfaceNormal * projGravOnNormal.magnitude;
    }

    // f_sliding = f_gravity + f_normal
    public static Vector3 Sliding(Vector3 forceGravity, Vector3 forceNormal)
    {
        // combined forces of gravity and normal
        return forceGravity + forceNormal;
    }

    // f_friction_s = -f_opposing if less than max, else -coeff*f_normal (max amount is coeff*|f_normal|)
    public static Vector3 FrictionStatic(Vector3 forceNormal, Vector3 forceOpposing, float frictionCoefficientStatic)
    {
        // max force friction can apply is coeff * normal force
        var maxForceFriction = frictionCoefficientStatic * forceNormal.magnitude;
        var opposingMag = forceOpposing.magnitude;

        // no friction can be applied if no opposing force exists
        if (opposingMag == 0) return Vector3.zero;

        // if opposing is less than (cannot overpower) max force friction,
        //     stop motion with inverse of opposing force
        if (opposingMag < maxForceFriction)
        {
            return -forceOpposing;
        }
        // elsewise, apply max force friction in inverse direction of normalized opposing force
        return -forceOpposing / opposingMag * -maxForceFriction;
    }

    // f_friction_k = -coeff*|f_normal| * unit(vel)
    public static Vector3 FrictionKinetic(Vector3 forceNormal, Vector3 particleVelocity, float frictionCoefficientKinetic)
    {
        return -frictionCoefficientKinetic * forceNormal.magnitude * particleVelocity.normalized;
    }

    public static Vector3 FrictionKinetic(float impulseNormal, Vector3 particleVelocity, float frictionCoefficientKinetic)
    {
        return -frictionCoefficientKinetic * impulseNormal * particleVelocity.normalized;
    }

    // f_drag = (p * u^2 * area * coeff)/2
    public static Vector3 Drag(Vector3 particleVelocity, Vector3 fluidVelocity, float fluidDensity, float objectAreaCrossSection, float objectDragCoefficient)
    {
        var relativeVelocity = particleVelocity - fluidVelocity;

        // relative vel^2, so drag increases exponentially with relative vel
        var relVelSqrMag = relativeVelocity.sqrMagnitude;
        // if relative velocity is 0, no drag exerted
        if (relVelSqrMag == 0) return Vector3.zero;

        var dragMagnitude = 0.5f * fluidDensity * objectAreaCrossSection * objectDragCoefficient * relVelSqrMag;
        // force in inverse direction of normalized velocity at calculated drag magnitude
        return -relativeVelocity / Mathf.Sqrt(relVelSqrMag) * dragMagnitude;
    }

    // f_spring = -coeff*(spring length - spring resting length)
    public static Vector3 Spring(Vector3 particlePosition, Vector3 anchorPosition, float springRestingLength, float springStiffnessCoefficient)
    {
        // Hooke's Law in 3D: F = -k * ( |d| - l0 ) * unit(d)
        // d = particle pos - spring anchor pos
        var springDir = particlePosition - anchorPosition;
        var springLength = springDir.sqrMagnitude;

        // optimization, no force if spring length is zero
        if (springLength == 0) return Vector3.zero;
        // sqrt to get actual spring length
        springLength = Mathf.Sqrt(springLength);

        var forceSpringMagnitude = -springStiffnessCoefficient * (springLength - springRestingLength);
        // force at magnitude in direction of normalized spring dir
        return springDir / springLength * forceSpringMagnitude;
    }
}
