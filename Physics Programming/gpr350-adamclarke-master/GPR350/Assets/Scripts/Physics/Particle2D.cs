﻿using System;
using UnityEngine;

public enum PositionIntegrationMethod
{
    EulerExplicit,
    Kinematic
}

public enum RotationIntegrationMethod
{
    EulerExplicit,
    Kinematic
}

public class Particle2D : MonoBehaviour
{
    public PositionIntegrationMethod positionIntegrationMethod;
    public RotationIntegrationMethod rotationIntegrationMethod;

    // lab 1 step 1 : define particle
    public Vector2 position;
    public Vector2 velocity;
    public Vector2 acceleration;
    public float maxAcceleration;
    public float maxVelocity;

    // rotation / "orientation" in degrees
    public float rotation;
    public float angularVelocity;
    public float angularAcceleration;

    // lab 2 step 1: mass
    public float startingMass;
    private float mass;
    private float massInv;
    public float Mass
    {
        set
        {
            // mass must be positive
            mass = Mathf.Max(0.0f, value);
            // if mass is 0, set inverse to 0 instead of infinity
            massInv = mass > 0.0f ? 1.0f / mass : 0.0f;
        }
        get => mass;
    }
    //private Vector2 localCenterOfMass;

    // lab 3 step 1: moment of intertia
    private float inertia;
    public float inertiaInv { get; private set; }
    public float Inertia
    {
        set
        {
            inertia = value;
            // 0 if inertia is 0, 1/inverse otherwise
            inertiaInv = inertia != 0 ? 1.0f / inertia : 0;
        }
        get => inertia;
    }

    public float gravityConstant = -9.8f;
    public Vector2 gravityUpDir = Vector2.up;

    // lab 2 step 2: force
    private Vector2 acumForce;

    // lab 3 step 2
    public float acumTorque;
    public Shape shape;

    // normal
    public Vector2 platformSurfaceNormal = new Vector2(-0.5f, 0.8660254f);
    public Vector2 platformContactLocalPoint = new Vector2(Mathf.Sqrt(2) / 2f, -Mathf.Sqrt(2) / 2f);
    //// friction
    public float frictionCoefficientStatic = 0.5f;
    public float frictionCoefficientKinetic = 0.5f;
    //// drag
    //public float dragCoefficient = 1f;
    //public Vector2 dragFluidVelocity = Vector2.zero;
    //public float dragFluidDensity = 1f;
    //// spring
    //public Transform springAnchorTransform;
    //public float springRestingLength = 0.5f;
    //public float springStiffnessCoeff = 1.0f;

    // lab 3 bonus
    //public bool bonus = false;

    private void Start()
    {
        // set intial position to level editor inital position
        position = transform.position;
        // set initial rotation to level editor inital rotation
        rotation = transform.rotation.eulerAngles.z;

        Mass = startingMass;

        // moment of inertia based on shape
        switch (shape)
        {
            case Shape.Disc:
                Inertia = MomentOfInteria.Disc(Mass, 0.1f);
                break;
            case Shape.Ring:
                Inertia = MomentOfInteria.Ring(Mass, 0.08f, 0.1f);
                break;
            case Shape.Rectangle:
                Inertia = MomentOfInteria.Rectangle(Mass, new Vector2(0.1f, 0.1f));
                break;
            case Shape.Rod:
                Inertia = MomentOfInteria.Rod(Mass, .2f);
                break;
        }

        //if (!bonus)
        //{
        //    AddForceAtLocalPoint(new Vector2(-5.0f, 0.0f), new Vector2(1, 1));
        //    AddForceAtLocalPoint(new Vector2(5.0f, 0.0f), new Vector2(-1, -1));
        //}
    }

    // lab 1step 3: update
    private void FixedUpdate()
    {
        // cap velocity
        float velMag = velocity.magnitude;
        // if exceeding max vel
        if (velMag > maxVelocity)
        {
            // normalize
            velocity /= velMag;
            // give magnitude of max vel
            velocity *= maxVelocity;
        }

        // integrate position
        if (positionIntegrationMethod == PositionIntegrationMethod.EulerExplicit)
        {
            UpdatePositionEulerExplicit(Time.fixedDeltaTime);
        }
        else
        {
            UpdatePositionKinematic(Time.fixedDeltaTime);
        }
        // give graphics access to position
        transform.position = position;

        // integrate rotation
        if (rotationIntegrationMethod == RotationIntegrationMethod.EulerExplicit)
        {
            UpdateRotationEulerExplicit(Time.fixedDeltaTime);
        }
        else
        {
            UpdateRotationKinematic(Time.fixedDeltaTime);
        }
        // give graphics access to rotation
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, rotation));

        // apply forces after integration
        UpdateAcceleration();
        UpdateAngularAcceleration();
    }

    public void AddForce(Vector2 f)
    {
        // D'Alembert
        acumForce += f;
    }

    public void AddTorque(float t)
    {
        // D'Alembert
        acumTorque += t;
    }

    public void AddForceAtWorldPoint(Vector2 force, Vector2 worldPoint)
    {
        AddForce(force);

        var momentArm = worldPoint - position;
        // Newton 2 for 2d torque
        // T = PxFy - PyFx
        float torque = (momentArm.x * force.y) - (momentArm.y * force.x);
        AddTorque(torque);
    }

    public void AddForceAtLocalPoint(Vector2 force, Vector2 localPoint)
    {
        localPoint += position;
        AddForceAtWorldPoint(force, localPoint);
    }

    public void AddImpulse(Vector2 impulse)
    {
        velocity += massInv * impulse;
    }

    public void Move(Vector2 move)
    {
        position += move;
    }

    private void UpdateAcceleration()
    {
        // Netwon 2
        acceleration = acumForce * massInv;

        // cap acceleration
        float accMag = acceleration.magnitude;
        // if exceeding max accel
        if (accMag > maxAcceleration)
        {
            // normalize
            acceleration /= accMag;
            // give magnitude of max accel
            acceleration *= maxAcceleration;
        }

        // clear forces
        acumForce = Vector2.zero;
    }

    private void UpdateAngularAcceleration()
    {
        // Newton 2
        angularAcceleration = inertiaInv * acumTorque;
        // clear torque
        acumTorque = 0;
    }

    // lab 1 step 2: integration algorithms
    private void UpdatePositionEulerExplicit(float dt)
    {
        // Euler's method:
        // F(t+dt) = F(t) + f(t)dt
        //                + (dF/dt)dt
        //                + dF

        // update position w/ first-order of displacement (explicit Euler formula)
        // x(t+dt) = x(t) + v(t)dt
        position += velocity * dt;

        // update velocity
        // v(t+dt) = v(t) + a(t)dt
        velocity += acceleration * dt;
    }

    private void UpdatePositionKinematic(float dt)
    {
        // update postion w/ first & second order of displacement (kinematic Euler formula)
        // x(t+dt) = x(t) + v(t)dt + 0.5 * a(t)dt^2
        position += velocity * dt + acceleration * (0.5f * dt * dt);

        // update velocity
        // v(t+dt) = v(t) + a(t)dt
        velocity += acceleration * dt;
    }

    private void UpdateRotationEulerExplicit(float dt)
    {
        // notation
        // r = rotationZ
        // r' = dr/dt
        // r'' = d^2 * r / d * t^2

        // dr = r(t) + r'(t)dt
        rotation = rotation + angularVelocity * dt;
        rotation = Mafs.MapToUnsigned360(rotation);

        // r'(t+dt) = r''(t)dt
        angularVelocity += angularAcceleration * dt;
    }

    private void UpdateRotationKinematic(float dt)
    {
        // notation
        // r = "rotationZ"
        // r' = dr/dt "angularVelocity"
        // r'' = d^2 * r / d * t^2 "angularAcceleration"

        // dr = r(t) + r'(t)dt + 0.5 * r''(t)dt^2
        rotation = rotation + angularVelocity * dt + angularAcceleration * (0.5f * dt * dt);
        rotation = Mafs.MapToUnsigned360(rotation);

        // r'(t+dt) = r''(t)dt
        angularVelocity += angularAcceleration * dt;
    }
}
