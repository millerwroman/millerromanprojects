﻿using System;

using UnityEngine;
using Matrix4x4 = UnityEngine.Matrix4x4;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;
using Vector4 = UnityEngine.Vector4;

namespace Yeet
{

    public enum PositionIntegrationMethod3D
    {
        EulerExplicit,
        Kinematic
    }

    public enum RotationIntegrationMethod3D
    {
        EulerExplicit,
        Kinematic
    }

    [Serializable]
    public struct ForceAtLocalPoint
    {
        public Vector3 force;
        public Vector3 localPoint;
    }

    public class Rigidbody : MonoBehaviour
    {


        #region Not in Inspector
        [HideInInspector]
        public Vector3 position;
        [HideInInspector]
        public Vector3 velocity;
        [HideInInspector]
        public Vector3 acceleration;

        // rotation / "orientation" in degrees
        [HideInInspector]
        public Yeet.Quaternion rotation;
        [HideInInspector]
        public Vector3 angularVelocity;
        [HideInInspector]
        public Vector3 angularAcceleration;

        [HideInInspector]
        public float MassInv => massInv;

        [HideInInspector]
        public Vector3 acumTorque;
        [HideInInspector]
        public ForceAtLocalPoint[] initForceAtLocalPoints = { };

        [HideInInspector]
        public Vector3 worldCenterOfMass;

        private float mass;
        private float massInv;
        #endregion

        #region Show In Inspector
        [Header("Integration Methods")]
        public PositionIntegrationMethod3D positionIntegrationMethod;
        public RotationIntegrationMethod3D rotationIntegrationMethod;

        [Header("Collision Response")]
        public float restitution = 1.0f;
        //public float coeffFriction = 1.0f;
        [Header("Position Constraints")]
        public bool freezePosX;
        public bool freezePosY;
        public bool freezePosZ;

        [Header("Body Values")]
        public Shape3D shape;
        public float startingMass;
        public Vector3 localCenterOfMass;

        #endregion




        public float Mass
        {
            set
            {
                // mass must be positive
                mass = Mathf.Max(0.0f, value);
                // if mass is 0, set inverse to 0 instead of infinity
                massInv = mass > 0.0f ? 1.0f / mass : 0.0f;
            }
            get => mass;
        }



        private Vector3 acumForce;



        public Matrix4x4 WorldTransformMat
        {
            get => worldTransformMat;
            set
            {
                worldTransformMat = value;
                worldTransformMatInv = worldTransformMat.inverse;
            }
        }

        private Matrix4x4 worldTransformMat;
        [HideInInspector]
        public Matrix4x4 worldTransformMatInv;


        private Matrix4x4 localInertia;
        private Matrix4x4 worldInertia;
        private Matrix4x4 localInertiaInv;

        public Matrix4x4 LocalIntertia
        {
            set
            {
                localInertia = value;
                // optimized inverse of scalar matrix
                localInertiaInv = new Matrix4x4(
                    new Vector4(1f / localInertia.m00, 0, 0, 0),
                    new Vector4(0, 1f / localInertia.m11, 0, 0),
                    new Vector4(0, 0, 1f / localInertia.m22, 0),
                    new Vector4(0, 0, 0, 1)
                );
                //localInertiaInv = localInertia.inverse;
            }
            get => localInertia;
        }


        private void Start()
        {
            // set intial position to level editor inital position
            position = transform.position;
            // set initial rotation to level editor inital rotation
            rotation = transform.rotation;

            Mass = startingMass;

            // moment of inertia based on shape and mass
            switch (shape)
            {
                case Shape3D.SolidBox:
                    LocalIntertia = MomentOfInteria.SolidBox(Mass, new Vector3(1, 1, 1));
                    break;
                case Shape3D.HollowBox:
                    LocalIntertia = MomentOfInteria.HollowBox(Mass, new Vector3(2, 1, 1));
                    break;
                case Shape3D.SolidSphere:
                    LocalIntertia = MomentOfInteria.SolidSphere(Mass, 0.5f);
                    break;
                case Shape3D.HollowSphere:
                    LocalIntertia = MomentOfInteria.HollowSphere(Mass, 0.5f);
                    break;
                case Shape3D.SolidCylinder:
                    LocalIntertia = MomentOfInteria.SolidCylinder(Mass, 0.5f, 2);
                    break;
                case Shape3D.SolidCone:
                    LocalIntertia = MomentOfInteria.SolidCone(Mass, 0.5f, 1.0f);
                    break;
            }

            foreach (var initForceAtLocalPoint in initForceAtLocalPoints)
            {
                AddForceAtLocalPoint(initForceAtLocalPoint.force, initForceAtLocalPoint.localPoint);
            }
        }

        private void FixedUpdate()
        {
            // allow changing transform in editor (requires collisionmanager update graphics transform)
            Transform pTransform = transform;
            position = pTransform.position;
            rotation = pTransform.rotation;

            // no forces or integration for kinematic bodies

            Vector3 prevPos = position;
            Quaternion prevRot = rotation;

            // integrate position
            if (positionIntegrationMethod == PositionIntegrationMethod3D.EulerExplicit)
            {
                UpdatePositionEulerExplicit(Time.fixedDeltaTime);
            }
            else
            {
                UpdatePositionKinematic(Time.fixedDeltaTime);
            }

            // integrate rotation
            if (rotationIntegrationMethod == RotationIntegrationMethod3D.EulerExplicit)
            {
                UpdateRotationEulerExplicit(Time.fixedDeltaTime);
            }
            else
            {
                //UpdateRotationKinematic(Time.fixedDeltaTime);
            }

            // constraints
            ApplyConstraints(prevPos, prevRot);

            // graphics transform
            UpdateTransform();

            // apply forces after integration
            UpdateAcceleration();
            UpdateAngularAcceleration();
        }

        private void UpdateAcceleration()
        {
            // Netwon 2
            acceleration = acumForce * massInv;
            // clear forces
            acumForce = Vector2.zero;
        }

        private void UpdateAngularAcceleration()
        {
            // rotation squared
            Vector4 rotSqr = new Vector4(rotation.X * rotation.X, rotation.Y * rotation.Y, rotation.Z * rotation.Z,
                rotation.W * rotation.W);
            // update world transformation matrix
            // R = current rotation quaternion as matrix, T  = current position
            // R1,1 R1,2 R1,3 T1
            // R2,1 R2,2 R2,3 T2 
            // R3,1 R3,2 R3,3 T3
            // 0    0    0    1
            WorldTransformMat = new Matrix4x4(
                new Vector4(rotSqr.w + rotSqr.x - rotSqr.y - rotSqr.z, 2 * (rotation.X * rotation.Y + rotation.W * rotation.Z), 2 * (rotation.X * rotation.Z - rotation.W * rotation.Y), 0),
                new Vector4(2 * (rotation.X * rotation.Y - rotation.W * rotation.Z), rotSqr.w - rotSqr.x + rotSqr.y - rotSqr.z, 2 * (rotation.Y * rotation.Z + rotation.W * rotation.X), 0),
                new Vector4(2 * (rotation.X * rotation.Z + rotation.W * rotation.Y), 2 * (rotation.Y * rotation.Z - rotation.W * rotation.X), rotSqr.w - rotSqr.x - rotSqr.y + rotSqr.z, 0),
                new Vector4(position.x, position.y, position.z, 1));
            // I(t)^-1 = R * I^-1 * R^-1
            worldInertia = worldTransformMat * localInertiaInv * worldTransformMatInv;

            // Newton 2
            // a(t) = I(t)^-1 * T(t)
            angularAcceleration = worldInertia * acumTorque;
            // clear acumulated torque
            acumTorque = Vector3.zero;
        }

        // lab 1 step 2: integration algorithms
        private void UpdatePositionEulerExplicit(float dt)
        {
            // Euler's method:
            // F(t+dt) = F(t) + f(t)dt
            //                + (dF/dt)dt
            //                + dF

            // update position w/ first-order of displacement (explicit Euler formula)
            // x(t+dt) = x(t) + v(t)dt
            position += velocity * dt;

            // update velocity
            // v(t+dt) = v(t) + a(t)dt
            velocity += acceleration * dt;
        }

        private void UpdatePositionKinematic(float dt)
        {
            // update postion w/ first & second order of displacement (kinematic Euler formula)
            // x(t+dt) = x(t) + v(t)dt + 0.5 * a(t)dt^2
            position += velocity * dt + acceleration * (0.5f * dt * dt);

            // update velocity
            // v(t+dt) = v(t) + a(t)dt
            velocity += acceleration * dt;
        }

        private void UpdateRotationEulerExplicit(float dt)
        {
            // delta rotation = 0.5 * angular velocity * current rotation * delta time
            // q(t+dt) = q(t) + (d/dt)q(t)
            // (d/dt)q(t) = 0.5 * w(t) * q(t) * dt
            Vector3 halfAngularVelocity = dt * 0.5f * angularVelocity;
            Yeet.Quaternion deltaRot = halfAngularVelocity * rotation;

            rotation += deltaRot;
            // normalize so scale is not applied
            rotation.Normalize();

            // r'(t+dt) = r''(t)dt
            angularVelocity += angularAcceleration * dt;
        }

        //private void UpdateRotationKinematic(float dt)
        //{
        //    // notation
        //    // r = "rotationZ"
        //    // r' = dr/dt "angularVelocity"
        //    // r'' = d^2 * r / d * t^2 "angularAcceleration"

        //    // dr = r(t) + r'(t)dt + 0.5 * r''(t)dt^2
        //    rotation = rotation + angularVelocity * dt + angularAcceleration * (0.5f * dt * dt);
        //    rotation = Mafs.MapToUnsigned360(rotation);

        //    // r'(t+dt) = r''(t)dt
        //    angularVelocity += angularAcceleration * dt;
        //}

        public void AddForce(Vector3 f)
        {
            // D'Alembert
            acumForce += f;
        }

        public void AddTorque(Vector3 t)
        {
            // D'Alembert
            acumTorque += t;
        }

        public void AddForceAtWorldPoint(Vector3 force, Vector3 worldPoint)
        {
            AddForce(force);

            var momentArm = worldPoint - position;
            // T = r X F
            AddTorque(Vector3.Cross(momentArm, force));
        }

        public void AddForceAtLocalPoint(Vector3 force, Vector3 localPoint)
        {
            localPoint += position;
            AddForceAtWorldPoint(force, localPoint);
        }

        public void AddImpulse(Vector3 impulse)
        {
            velocity += massInv * impulse;
        }

        public void Move(Vector3 move)
        {
            position += move;
        }

        public void ApplyConstraints(Vector3 prevPos, Quaternion prevRot)
        {
            if (freezePosX) position.x = prevPos.x;
            if (freezePosY) position.y = prevPos.y;
            if (freezePosZ) position.z = prevPos.z;
            // TODO:
            // get rotation as eurler angles to lock eurler angles :(
            //Vector3 prevEulerAngles = prevRot;
            //Vector3 eulerAngles = rotation;
            //if (freezeRotX) eulerAngles.x = prevEulerAngles.x;
            //if (freezeRotY) eulerAngles.y = prevEulerAngles.y;
            //if (freezeRotZ) eulerAngles.z = prevEulerAngles.z;
            //rotation = eulerAngles;
        }

        // give graphics access to position & rotation
        public void UpdateTransform()
        {
            Transform pTransform = transform;
            pTransform.position = position;
            pTransform.rotation = rotation;
        }
    }
}