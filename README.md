# MillerRomanProjects
[LinkedIn](https://www.linkedin.com/in/millerwroman/)   
Email: <Millerwroman@gmail.com>
Mobile: (720) 633-2855

## My most current work is an ongoing Capstone Project that is avlible at request. Due to the size of the folder it is located in its independent repository. 

## Animation Programming
This is a series of videos located in the "Animation Programming" folder. The Project itself in located in the "Animation Programming Project" folder.
### Project Description: 
This is a current ongoing project about creating a 3d animation system in Unreal Engine 4. Starting from a blank project we are
implementing pose to pose animations using a variety of interpolation types to smoothly transition between keyframes. This project includes
the ability to read in HTR files and create an animation skeleton using the file's data. As we continue to work, the functionally will expand.
#### Team:
Programmer: Miller Roman
Programmer: Adam Clarke (Adam.Clarke@mymail.champlain.edu)

## Physics Programming
This is located in the "Physics Programming" Folder. It includes a Unity project and a DLL for it.
### Project Description: 
This project was a project that we contiued to add funtionality on thought the fall semester 2019. It is a build up starting in 2D and moving to 3D
of simulating physics in Unity engine. This does not use the built in physics system but instead custom rigidbodys and colliders made by my team. 
As the project progressed we also added force simulators (gravity, friction, normal force, etc..). 
#### Team:
Programmer: Miller Roman
Programmer: Adam Clarke (Adam.Clarke@mymail.champlain.edu)

