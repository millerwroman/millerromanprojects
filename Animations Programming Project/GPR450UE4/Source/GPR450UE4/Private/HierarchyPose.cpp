// Fill out your copyright notice in the Description page of Project Settings.


#include "HierarchyPose.h"
#include "Hierarchy.h"

HierarchyPose::HierarchyPose(uint32 numNodes)
{
	Pose.SetNumUninitialized(numNodes);
	for (uint32 i = 0; i < numNodes; i++)
	{
		Pose[i] = new SpatialPose(SpatialPose::Construct(FTransform::Identity));
	}
}

//HierarchyPose::HierarchyPose(const HierarchyPose& rhs)
//{
//}

HierarchyPose::~HierarchyPose()
{
	for (int32 i = 0; i < Pose.Num(); i++)
	{
		delete Pose[i];
	}
}

void HierarchyPose::Reset(const uint32 nodeCount)
{
	Pose.Empty();
}

HierarchyPose* HierarchyPose::Step(HierarchyPose* p_out, HierarchyPose* p0)
{
	for (int i = 0; i < p_out->Pose.Num(); i++)
	{
		p_out->Pose[i] = p0->Pose[i];
	}
	return p_out;
}

HierarchyPose* HierarchyPose::Nearest(HierarchyPose* p_out, HierarchyPose* p0, HierarchyPose* p1, float u)
{
	for (int i = 0; i < p_out->Pose.Num(); i++)
	{
		SpatialPose::Nearest(p_out->Pose[i], p0->Pose[i], p1->Pose[i], u);
	}
	return p_out;
}

HierarchyPose* HierarchyPose::Linear(HierarchyPose* p_out, HierarchyPose* p0, HierarchyPose* p1, float u)
{
	for (int i = 0; i < p_out->Pose.Num(); i++)
	{
		SpatialPose::Lerp(p_out->Pose[i], p0->Pose[i], p1->Pose[i], u);
	}
	return p_out;
}

HierarchyPose* HierarchyPose::Smoothstep(HierarchyPose* p_out, HierarchyPose* p0, HierarchyPose* p1, float u)
{
	for (int i = 0; i < p_out->Pose.Num(); i++)
	{
		SpatialPose::Smoothstep(p_out->Pose[i], p0->Pose[i], p1->Pose[i], u);
	}
	return p_out;
}

HierarchyPose* HierarchyPose::Smootherstep(HierarchyPose* p_out, HierarchyPose* p0, HierarchyPose* p1, float u)
{
	for (int i = 0; i < p_out->Pose.Num(); i++)
	{
		SpatialPose::Smootherstep(p_out->Pose[i], p0->Pose[i], p1->Pose[i], u);
	}
	return p_out;
}

HierarchyPose* HierarchyPose::Cubic(HierarchyPose* p_out, HierarchyPose* pP, HierarchyPose* p0, HierarchyPose* p1, HierarchyPose* pN, float u)
{
	for (int i = 0; i < p_out->Pose.Num(); i++)
	{
		SpatialPose::Cubic(p_out->Pose[i], pP->Pose[i], p0->Pose[i], p1->Pose[i], pN->Pose[i], u);
	}
	return p_out;
}

HierarchyPose* HierarchyPose::ForwardKinematics(HierarchyPose* p_world_out, HierarchyPose* p_obj_out, HierarchyPose* p_local, Hierarchy const* p_hierarchy,
	SpatialPose* parentActorPose)
{
	//	- for all nodes starting at first index
	//		- if node is not root (has parent node)
	//			- object matrix = parent object matrix * local matrix
	//		- else
	//			- copy local matrix to object matrix
	for (int32 i = 0; i < p_hierarchy->Nodes.Num(); ++i)
	{
		HierarchyNode* node = p_hierarchy->Nodes[i];
		// if not a root node
		if (node->ParentIndex >= 0)
		{
			//FTransform::Multiply(&ObjectSpacePose->Pose[i]->Transform, &LocalSpacePose->Pose[i]->Transform, &ObjectSpacePose->Pose[node->ParentIndex]->Transform);
			SpatialPose::ForwardKinematics(p_obj_out->Pose[i],
				p_local->Pose[i], p_obj_out->Pose[node->ParentIndex]);
		}
		// is root node
		else
		{
			SpatialPose::Copy(p_obj_out->Pose[i], p_local->Pose[i]);
		}
		SpatialPose::ForwardKinematics(p_world_out->Pose[i],
			p_obj_out->Pose[i], parentActorPose);
	}
	return p_obj_out;
}

HierarchyPose* HierarchyPose::PartialInverseKinematics(HierarchyPose* p_local_out, HierarchyPose* p_obj, Hierarchy const* hierarchy,
	SpatialPose* worldSpaceTarget, int32 startNode, int32 endNode, HierarchyPose* p_base)
{
	check(startNode >= 0);
	check(endNode < hierarchy->Nodes.Num());


	for (int32 i = startNode; i <= endNode; ++i)
	{
		HierarchyPose* temp = new HierarchyPose(64);
		HierarchyNode* node = hierarchy->Nodes[i];
		if (i == startNode)
		{
			SpatialPose::InverseKinematics(temp->Pose[i], p_obj->Pose[i], worldSpaceTarget);
		}
		else
		{
			SpatialPose::InverseKinematics(p_local_out->Pose[i],
				p_obj->Pose[i], p_obj->Pose[node->ParentIndex]);
		}
		SpatialPose::Revert(temp->Pose[i]);
		SpatialPose::Deconcat(p_local_out->Pose[i], p_base->Pose[i], temp->Pose[i]);
		SpatialPose::Convert(p_local_out->Pose[i]);

		//HierarchyNode* node = hierarchy->Nodes[i];
		//if (i == startNode)
		//{
		//	SpatialPose::InverseKinematics(p_local_out->Pose[i], p_obj->Pose[i], worldSpaceTarget);
		//}
		//else
		//{
		//	HierarchyPose* temp = new HierarchyPose(64);
		//	SpatialPose::InverseKinematics(p_local_out->Pose[i],
		//		p_obj->Pose[i], p_obj->Pose[node->ParentIndex]);
		//	//SpatialPose::Revert(temp->Pose[i]);
		//	//SpatialPose::Deconcat(p_local_out->Pose[i], p_base->Pose[i], temp->Pose[i]);
		//}
		//SpatialPose::Convert(p_local_out->Pose[i]);
	}
	return p_local_out;
}


HierarchyPose* HierarchyPose::Convert(HierarchyPose* p_out)
{
	for (int i = 0; i < p_out->Pose.Num(); i++)
	{
		SpatialPose::Convert(p_out->Pose[i]);
	}
	return p_out;
}

HierarchyPose* HierarchyPose::Concat(HierarchyPose* p_out, HierarchyPose* p0, HierarchyPose* p1)
{
	for (int i = 0; i < p_out->Pose.Num(); i++)
	{
		SpatialPose::Concat(p_out->Pose[i], p0->Pose[i], p1->Pose[i]);
	}
	return p_out;
}

HierarchyPose* HierarchyPose::Triangular(HierarchyPose* p_out, HierarchyPose* p0, HierarchyPose* p1, HierarchyPose* p2,
	float u1, float u2)
{
	for (int i = 0; i < p_out->Pose.Num(); i++)
	{
		SpatialPose::Triangular(p_out->Pose[i], p0->Pose[i], p1->Pose[i], p2->Pose[i], u1, u2);
	}
	return p_out;
}

HierarchyPose* HierarchyPose::BiNearest(HierarchyPose* p_out, HierarchyPose* p0, HierarchyPose* p1, HierarchyPose* p2,
	HierarchyPose* p3, float u0, float u1, float u)
{
	for (int i = 0; i < p_out->Pose.Num(); i++)
	{
		SpatialPose::BiNearest(p_out->Pose[i], p0->Pose[i], p1->Pose[i], p2->Pose[i], p3->Pose[i],
			u0, u1, u);
	}
	return p_out;
}


HierarchyPose* HierarchyPose::BiLinear(HierarchyPose* p_out, HierarchyPose* p0, HierarchyPose* p1, HierarchyPose* p2,
	HierarchyPose* p3, float u0, float u1, float u)
{
	for (int i = 0; i < p_out->Pose.Num(); i++)
	{
		SpatialPose::BiLinear(p_out->Pose[i], p0->Pose[i], p1->Pose[i], p2->Pose[i], p3->Pose[i],
			u0, u1, u);
	}
	return p_out;
}

HierarchyPose* HierarchyPose::BiCubic(HierarchyPose* p_out, HierarchyPose* p0, HierarchyPose* p1, HierarchyPose* p2,
	HierarchyPose* p3, HierarchyPose* p4, HierarchyPose* p5, HierarchyPose* p6, HierarchyPose* p7, HierarchyPose* p8,
	HierarchyPose* p9, HierarchyPose* p10, HierarchyPose* p11, HierarchyPose* p12, HierarchyPose* p13,
	HierarchyPose* p14, HierarchyPose* p15, float u0, float u1, float u2, float u3,
	float u)
{
	for (int i = 0; i < p_out->Pose.Num(); i++)
	{
		SpatialPose::BiCubic(p_out->Pose[i],
			p0->Pose[i], p1->Pose[i], p2->Pose[i], p3->Pose[i],
			p4->Pose[i], p5->Pose[i], p6->Pose[i], p7->Pose[i],
			p8->Pose[i], p9->Pose[i], p10->Pose[i], p11->Pose[i],
			p12->Pose[i], p13->Pose[i], p14->Pose[i], p15->Pose[i],
			u0, u1, u2, u3, u);
	}
	return p_out;
}


