// Fill out your copyright notice in the Description page of Project Settings.


#include "KeyframePool.h"

KeyframePool* KeyframePool::instance = nullptr;

KeyframePool::KeyframePool()
{
	// singleton
	if (KeyframePool::instance == nullptr)
	{
		KeyframePool::instance = this;
	}
	else
	{
		return;
	}
}

KeyframePool::~KeyframePool()
{
	int num = Keyframes.Num();
	for (int i = 0; i < num; i++)
	{
		delete Keyframes[i];
	}
	Keyframes.Empty();
	if (KeyframePool::instance == this)
	{
		KeyframePool::instance = nullptr;
	}
}

KeyframePool* KeyframePool::GetInstance()
{
	if (instance == nullptr)
	{
		instance = new KeyframePool();
	}
	return instance;
}

Keyframe* KeyframePool::GetKeyframe(int32 index)
{
	check(index < Keyframes.Num())
	return Keyframes[index];
}

int32 KeyframePool::AddKeyframe(Keyframe* newKeyframe)
{
	Keyframes.Add(newKeyframe);
	return Keyframes.Num() - 1;
}
