// Fill out your copyright notice in the Description page of Project Settings.


#include "HierarchyPoseGroup.h"
#include "KeyframePool.h"
#include "ClipPool.h"

HierarchyPoseGroup::HierarchyPoseGroup(Hierarchy* hierarchy)
	:HierarchyPtr(hierarchy)
{
	uint32 numHierarchyNodes = (uint32)HierarchyPtr->Nodes.Num();

	mpBasePose = new HierarchyPose(numHierarchyNodes);
}

HierarchyPoseGroup::~HierarchyPoseGroup()
{
	for (int32 i = 0; i < HierarchicalPoses.Num(); i++)
	{
		delete HierarchicalPoses[i];
	}
	delete HierarchyPtr;
}

int32 HierarchyPoseGroup::GetPoseOffsetIndex(const uint32 poseIndex)
{
	if (HierarchyPtr)
	{
		return (poseIndex * HierarchyPtr->Nodes.Num());
	}
	return -1;
}

int32 HierarchyPoseGroup::GetNodePoseOffsetIndex(const uint32 poseIndex, const uint32 nodeIndex)
{
	if (HierarchyPtr)
	{
		return (poseIndex * HierarchyPtr->Nodes.Num() + nodeIndex);
	}
	return -1;
}

SpatialPose* HierarchyPoseGroup::GetSpatialPose(int32 poseIndex, int32 nodeIndex)
{
	return HierarchicalPoses[poseIndex]->Pose[nodeIndex];
}

void HierarchyPoseGroup::AddNumUninitializedPoses(int32 num)
{
	int32 firstPoseIndex = HierarchicalPoses.Num();
	HierarchicalPoses.SetNumUninitialized(firstPoseIndex + num);
	for (int32 i = firstPoseIndex; i < HierarchicalPoses.Num(); i++)
	{
		HierarchicalPoses[i] = new HierarchyPose(HierarchyPtr->Nodes.Num());
	}
}

//void HierarchyPoseGroup::Triangular(HierarchyPose* deltaPose, int32 pose0Index, int32 pose1Index, int32 poseNIndex, float u)
//{
//	HierarchyPose* pose0 = HierarchicalPoses[pose0Index];
//	HierarchyPose* pose1 = HierarchicalPoses[pose1Index];
//	HierarchyPose* poseN = HierarchicalPoses[poseNIndex];
//	for (int i = 0; i < deltaPose->Pose.Num(); i++)
//	{
//		//SpatialPose::Triangular(deltaPose->Pose[i], pose0->Pose[i], pose1->Pose[i], poseN->Pose[i], u);
//	}
//}
