// Fill out your copyright notice in the Description page of Project Settings.


#include "SpatialPose.h"
#include "Math/UnrealMathUtility.h"
#include "Engine/World.h"

SpatialPose::SpatialPose()
{
}

SpatialPose::SpatialPose(const SpatialPose& rhs)
{
	Copy(this, &rhs);
}

SpatialPose::~SpatialPose()
{
}

//void SpatialPose::BeginPlay()
//{
//	//static ConstructorHelpers::FObjectFinder<UBlueprint> CubeMeshBP(TEXT("Blueprint'/Game/Items/Blueprints/BP_ItemTest.BP_ItemTest'"));
//	//UClass* CubeMeshClass = (UClass*)CubeMeshBP.Object->GeneratedClass;
//	//AddComponent()
//}

void SpatialPose::SetRotation(const FVector orientation)
{
	Orientation = orientation;
}

void SpatialPose::SetScale(const FVector scale)
{
	Scale = scale;
}

void SpatialPose::SetTranslation(const FVector translation)
{
	Translation = translation;
}

void SpatialPose::SetBoneLength(float boneLength)
{
	BoneLength = boneLength;
}

SpatialPose SpatialPose::Construct(FVector orientation, FVector translation, FVector scale)
{
	SpatialPose newPose;

	newPose.SetRotation(orientation);
	newPose.SetTranslation(translation);
	newPose.SetScale(scale);
	Convert(&newPose);

	return newPose;
}

SpatialPose SpatialPose::Construct(FTransform transform)
{
	SpatialPose p_out;
	p_out.Transform = transform;
	Revert(&p_out);
	return p_out;
}

SpatialPose* SpatialPose::Copy(SpatialPose* pose_out, const SpatialPose* p_rhs)
{
	check(pose_out != nullptr);

	pose_out->Transform = p_rhs->Transform;
	pose_out->Orientation = p_rhs->Orientation;
	pose_out->Translation = p_rhs->Translation;
	pose_out->Scale = p_rhs->Scale;

	return pose_out;
}

SpatialPose* SpatialPose::ResetToIdentity(SpatialPose* p_out)
{
	check(p_out);

	p_out->Transform.SetIdentity();
	p_out->Orientation = FVector::ZeroVector;
	p_out->Scale = FVector::OneVector;
	p_out->Translation = FVector::ZeroVector;

	return p_out;
}

SpatialPose* SpatialPose::InvertPiecewise(SpatialPose* p_out)
{
	check(p_out);

	p_out->Orientation *= -1;
	p_out->Translation *= -1;
	p_out->Scale = p_out->Scale.Reciprocal();


	return p_out;
}

SpatialPose* SpatialPose::InvertMatrix(SpatialPose* p_out)
{
	check(p_out);

	p_out->Transform = p_out->Transform.Inverse();
	
	return p_out;
}

SpatialPose* SpatialPose::Concat(SpatialPose* p_out, const SpatialPose* p_lhs, const SpatialPose* p_rhs)
{
	check(p_out);

	p_out->Orientation = p_lhs->Orientation + p_rhs->Orientation;
	p_out->Scale = p_lhs->Scale * p_rhs->Scale;
	p_out->Translation = p_lhs->Translation + p_rhs->Translation;

	return p_out;
}

SpatialPose* SpatialPose::Nearest(SpatialPose* p_out, const SpatialPose* p0, const SpatialPose* p1, float u)
{
	check(p_out);

	if (u < 0.5)
	{
		*p_out = *p0;
	}
	// >= 0.5
	else
	{
		*p_out = *p1;
	}

	return p_out;
}

SpatialPose* SpatialPose::Lerp(SpatialPose* p_out, const SpatialPose* p0, const SpatialPose* p1, float u)
{
	check(p_out);

	p_out->Orientation = FMath::Lerp(p0->Orientation, p1->Orientation, u);
	p_out->Scale = FMath::Lerp(p0->Scale, p1->Scale, u);
	p_out->Translation = FMath::Lerp(p0->Translation, p1->Translation, u);

	return p_out;
}

SpatialPose* SpatialPose::Smoothstep(SpatialPose* p_out, const SpatialPose* p0, const SpatialPose* p1, float u)
{
	check(p_out);

	// hermite polynomial spline, 1st order derivative of spline is 0 at edges
	float smoothU = u * u * (3 - 2 * u);

	p_out->Orientation = FMath::Lerp(p0->Orientation, p1->Orientation, smoothU);
	p_out->Scale = FMath::Lerp(p0->Scale, p1->Scale, smoothU);
	p_out->Translation = FMath::Lerp(p0->Translation, p1->Translation, smoothU);

	return p_out;
}

SpatialPose* SpatialPose::Smootherstep(SpatialPose* p_out, const SpatialPose* p0, const SpatialPose* p1, float u)
{
	check(p_out);

	// hermite polynomial w/ 1st & 2nd order derivative = 0 at edges
	float smootherU = u * u * u * (u * (u * 6 - 15) + 10);

	p_out->Orientation = FMath::Lerp(p0->Orientation, p1->Orientation, smootherU);
	p_out->Scale = FMath::Lerp(p0->Scale, p1->Scale, smootherU);
	p_out->Translation = FMath::Lerp(p0->Translation, p1->Translation, smootherU);

	return p_out;
}

SpatialPose* SpatialPose::Cubic(SpatialPose* p_out, const SpatialPose* pP, const SpatialPose* p0, const SpatialPose* p1, const SpatialPose* pN, float u)
{
	check(p_out);

	// cubic hermite, catmull-rom spline - https://en.wikipedia.org/wiki/Cubic_Hermite_spline#Catmull%E2%80%93Rom_spline
	// 0.5 * (((-pP + 3p0 - 3p1 + pN) * u + (2pP - 5p0 + 4p1 - pN)) * u + (-pP + p1)) * u + p0

	SpatialPose f0, f1, f01, f2, f012;

	// -pP
	SpatialPose pP_inv = *pP; InvertPiecewise(&pP_inv);
	// 3p0
	SpatialPose p0_3 = *p0; ScalePose(&p0_3, 3);
	// 3p1
	SpatialPose p1_3 = *p1; ScalePose(&p1_3, 3);
	Concat(&f0, &pP_inv, &p0_3);
	Deconcat(&f0, &f0, &p1_3);
	Concat(&f0, &f0, pN);
	// (-pP + 3p0 - 3p1 + pN) * u
	ScalePose(&f0, u);

	// 2pP
	SpatialPose pP_2 = *pP; ScalePose(&pP_2, 2);
	// 5p0
	SpatialPose p0_5 = *p0; ScalePose(&p0_5, 5);
	// 4p1
	SpatialPose p1_4 = *p1; ScalePose(&p1_4, 4);
	Deconcat(&f1, &pP_2, &p0_5);
	Concat(&f1, &f1, &p1_4);
	// (2pP - 5p0 + 4p1 - pN)
	Deconcat(&f1, &f1, pN);

	// ((-pP + 3p0 - 3p1 + pN) * u + (2pP - 5p0 + 4p1 - pN))
	Concat(&f01, &f0, &f1);
	// ((-pP + 3p0 - 3p1 + pN) * u + (2pP - 5p0 + 4p1 - pN)) * u
	ScalePose(&f01, u);

	// (-pP + p1)
	Concat(&f2, &pP_inv, p1);
	// (((-pP + 3p0 - 3p1 + pN) * u + (2pP - 5p0 + 4p1 - pN)) * u + (-pP + p1))
	Concat(&f012, &f01, &f2);
	// (((-pP + 3p0 - 3p1 + pN) * u + (2pP - 5p0 + 4p1 - pN)) * u + (-pP + p1)) * u
	ScalePose(&f012, u);
	// 0.5 * (((-pP + 3p0 - 3p1 + pN) * u + (2pP - 5p0 + 4p1 - pN)) * u + (-pP + p1)) * u
	ScalePose(&f012, 0.5);
	// 0.5 * (((-pP + 3p0 - 3p1 + pN) * u + (2pP - 5p0 + 4p1 - pN)) * u + (-pP + p1)) * u + p0
	Concat(p_out, &f012, p0);

	return p_out;
}

SpatialPose* SpatialPose::Convert(SpatialPose* p_out)
{
	check(p_out);

	p_out->Transform.SetIdentity();
	p_out->Transform.SetRotation(FQuat::MakeFromEuler(p_out->Orientation));
	p_out->Transform.SetScale3D(p_out->Scale);
	p_out->Transform.SetTranslation(p_out->Translation);

	return p_out;
}

SpatialPose* SpatialPose::Revert(SpatialPose* p_out)
{
	check(p_out);

	p_out->Translation = p_out->Transform.GetTranslation();
	p_out->Orientation = p_out->Transform.GetRotation().Euler();
	p_out->Scale = p_out->Transform.GetScale3D();

	return p_out;
}

SpatialPose* SpatialPose::Deconcat(SpatialPose* p_out, const SpatialPose* p_lhs, const SpatialPose* p_rhs)
{
	check(p_out);

	// p_lhs + p_rhs^-1
	// p0 + inv of p1
	SpatialPose inverse_rhs = *p_rhs;
	Concat(p_out, p_lhs, InvertPiecewise(&inverse_rhs));

	return p_out;
}

SpatialPose* SpatialPose::ScalePose(SpatialPose* p_out, float scalar)
{
	check(p_out);

	SpatialPose identityPose;
	ResetToIdentity(&identityPose);
	Lerp(p_out, &identityPose, p_out, scalar);

	return p_out;
}

SpatialPose* SpatialPose::Divide(SpatialPose* p_out, float divisor)
{
	check(p_out)

		// scale pose
		SpatialPose invPose;
	InvertPiecewise(Copy(&invPose, p_out));
	ScalePose(&invPose, divisor);

	return p_out;
}

SpatialPose* SpatialPose::Triangular(SpatialPose* p_out, SpatialPose* p0, SpatialPose* p1, SpatialPose* p2, float u1, float u2)
{
	check(p_out);

	float u0 = 1 - u1 - u2;
	// p = u0*p0 + u1*p1 + u2*p2
	Concat(p_out, ScalePose(p0, u0), ScalePose(p1, u1));
	Concat(p_out, p_out, ScalePose(p2, u2));

	return p_out;
}

SpatialPose* SpatialPose::BiNearest(SpatialPose* p_out, SpatialPose* p0, SpatialPose* p1, SpatialPose* p2, SpatialPose* p3, float u0, float u1, float u)
{
	check(p_out);

	SpatialPose p01, p23;
	Nearest(&p01, p0, p1, u0);
	Nearest(&p23, p2, p3, u1);
	Nearest(p_out, &p01, &p23, u);

	return p_out;
}


SpatialPose* SpatialPose::BiLinear(SpatialPose* p_out, SpatialPose* p0, SpatialPose* p1, SpatialPose* p2, SpatialPose* p3, float u0, float u1, float u)
{
	check(p_out);

	SpatialPose p01, p23;
	Lerp(&p01, p0, p1, u0);
	Lerp(&p23, p2, p3, u1);
	Lerp(p_out, &p01, &p23, u);

	return p_out;
}


SpatialPose* SpatialPose::BiCubic(SpatialPose* p_out, SpatialPose* p0, SpatialPose* p1, SpatialPose* p2, SpatialPose* p3, SpatialPose* p4, SpatialPose* p5, SpatialPose* p6, SpatialPose* p7, SpatialPose* p8, SpatialPose* p9, SpatialPose* p10, SpatialPose* p11, SpatialPose* p12, SpatialPose* p13, SpatialPose* p14, SpatialPose* p15, float u0, float u1, float u2, float u3, float u)
{
	check(p_out);

	SpatialPose p01, p23, p45, p67;
	Cubic(&p01, p0, p1, p2, p3, u0);
	Cubic(&p23, p4, p5, p6, p7, u1);
	Cubic(&p45, p8, p9, p10, p11, u2);
	Cubic(&p67, p12, p13, p14, p15, u3);
	Cubic(p_out, &p01, &p23, &p45, &p67, u);

	return p_out;
}

SpatialPose* SpatialPose::ForwardKinematics(SpatialPose* p_obj_out, SpatialPose* p_local, SpatialPose* p_parent_obj)
{
	check(p_obj_out);
	check(p_local);
	check(p_parent_obj);

	FTransform::Multiply(&p_obj_out->Transform,
		&p_local->Transform, &p_parent_obj->Transform);

	return p_obj_out;
}


SpatialPose* SpatialPose::InverseKinematics(SpatialPose* p_local_out, SpatialPose* p_obj, SpatialPose* p_parent_obj)
{
	check(p_local_out);
	check(p_obj);
	check(p_parent_obj);

	SpatialPose p_parent_obj_inv;
	Copy(&p_parent_obj_inv, p_parent_obj);
	InvertMatrix(&p_parent_obj_inv);

	// LocalN = ObjPinv * ObjN
	FTransform::Multiply(&p_local_out->Transform, &p_obj->Transform, &p_parent_obj_inv.Transform);

	return p_local_out;
}
