// Fill out your copyright notice in the Description page of Project Settings.


#include "HierarchyState.h"
#include "Kinematics.h"
#include "KeyframePool.h"
#include "Keyframe.h"

HierarchyState::HierarchyState(const HierarchyPoseGroup* hierarchyPoseGroup)
	: pHierarchyPoseGroup(hierarchyPoseGroup)
{
	NumNodes = pHierarchyPoseGroup->HierarchyPtr->Nodes.Num();
	DeltaPose = new HierarchyPose(NumNodes);
	LocalSpacePose = new HierarchyPose(NumNodes);
	ObjectSpacePose = new HierarchyPose(NumNodes);
	WorldSpacePose = new HierarchyPose(NumNodes);
}

HierarchyState::~HierarchyState()
{
}

void HierarchyState::UpdateObjectInverse()
{
}


void HierarchyState::Update(FKeyframeState& keyframeState, EPoseInterpolationMode poseInterpMode)
{
	//KeyframePool* keyframePool = KeyframePool::GetInstance();
	//Keyframe* keyframeP = keyframePool->GetKeyframe(keyframeState.KeyframePIndex);
	//Keyframe* keyframe0 = keyframePool->GetKeyframe(keyframeState.Keyframe0Index);
	//Keyframe* keyframe1 = keyframePool->GetKeyframe(keyframeState.Keyframe1Index);
	//Keyframe* keyframeN = keyframePool->GetKeyframe(keyframeState.KeyframeNIndex);

	//int32 pPIndex = keyframeP->GetValue();
	//int32 p0Index = keyframe0->GetValue();
	//int32 p1Index = keyframe1->GetValue();
	//int32 pNIndex = keyframeN->GetValue();

	//float u = keyframeState.KeyframeParameter;

	//// HIERARCHICAL POSE-TO-POSE ANIM TICK
	//// 1) interpolate -> between deltas/key
	//switch (poseInterpMode)
	//{
	//}

	//for (int i = 0; i < NumNodes; i++)
	//{
	//	// 2) concatenate -> current delta with base
	//	// concat delta if not base pose
	//	SpatialPose::Concat(LocalSpacePose->Pose[i], pHierarchyPoseGroup->GetBasePose()->Pose[i], DeltaPose->Pose[i]);

	//	// 3) convert - to homogenous matrix
	//	SpatialPose::Convert(LocalSpacePose->Pose[i]);
	//}

	//// 4) FK ->          local-space to object-space
	//KinematicsSolveForward();

	//for (int i = 0; i < NumNodes; i++)
	//{
	//	// apply to actors
	//	mJointActors[i]->SetActorTransform(ObjectSpacePose->Pose[i]->Transform);
	//}
}

