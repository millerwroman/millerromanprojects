// Fill out your copyright notice in the Description page of Project Settings.


#include "ClipPool.h"

ClipPool* ClipPool::instance = nullptr;

ClipPool::ClipPool()
{
	// singleton
	if (ClipPool::instance == nullptr)
	{
		ClipPool::instance = this;
	}
	else
	{
		return;
	}
}

ClipPool::~ClipPool()
{
	int num = Clips.Num();
	for (int i = 0; i < num; i++)
	{
		delete Clips[i];
	}
	Clips.Empty();
	if (ClipPool::instance == this)
	{
		ClipPool::instance = nullptr;
	}
}

ClipPool* ClipPool::GetInstance()
{
	if (instance == nullptr)
	{
		instance = new ClipPool();
	}
	return instance;
}

Clip* ClipPool::GetClip(int32 index)
{
	return Clips[index];
}

int32 ClipPool::AddClip(Clip* clip)
{
	Clips.Add(clip);
	return Clips.Num() - 1;

	/*Clips.Add(new UClip());
	Clips.Last()->Init("Clip0-3", 0, 3, KeyframePool);
	Clips.Add(new UClip());
	Clips.Last()->Init("Clip4-7", 4, 7, KeyframePool);
	Clips.Add(new UClip());
	Clips.Last()->Init("Clip8-11", 8, 11, KeyframePool);
	Clips.Add(new UClip());
	Clips.Last()->Init("Clip12-15", 12, 15, KeyframePool);
	Clips.Add(new UClip());
	Clips.Last()->Init("Clip16-19", 16, 19, KeyframePool);*/
}

int32 ClipPool::GetClipIndex(FString name)
{
	for (int i = 0; i < Clips.Num(); i++)
	{
		if (Clips[i]->GetName() == name)
		{
			return i;
		}
	}
	return -1;
}
