#include "PoseFrame.h"
#include "KeyframePool.h"


void PoseFrame::Update()
{
	KeyframePool* keyframePool = KeyframePool::GetInstance();
	Keyframe* keyframeP = keyframePool->GetKeyframe(mpKeyframeState->KeyframePIndex);
	Keyframe* keyframe0 = keyframePool->GetKeyframe(mpKeyframeState->Keyframe0Index);
	Keyframe* keyframe1 = keyframePool->GetKeyframe(mpKeyframeState->Keyframe1Index);
	Keyframe* keyframeN = keyframePool->GetKeyframe(mpKeyframeState->KeyframeNIndex);

	int32 pPIndex = keyframeP->GetValue();
	int32 p0Index = keyframe0->GetValue();
	int32 p1Index = keyframe1->GetValue();
	int32 pNIndex = keyframeN->GetValue();

	pP = mpHierarchyPoseGroup->HierarchicalPoses[pPIndex];
	p0 = mpHierarchyPoseGroup->HierarchicalPoses[p0Index];
	p1 = mpHierarchyPoseGroup->HierarchicalPoses[p1Index];
	pN = mpHierarchyPoseGroup->HierarchicalPoses[pNIndex];
}

