// Fill out your copyright notice in the Description page of Project Settings.


#include "HierarchyFileImporter.h"

HTRHeader HierarchyFileImporter::LoadHTR(FString filePath, HierarchyPoseGroup*& pHierarchyPoseGroup,
	bool loadMotionIntoBasePose)
{
	FString fullFilePath = FPaths::Combine(FPaths::ProjectContentDir(), filePath);

	FString fileContents;
	FFileHelper::LoadFileToString(fileContents, *fullFilePath);
	TArray<FString> sections;
	int32 sectionCount = fileContents.ParseIntoArray(sections, _T("["), true);

	Hierarchy* pHierarchy = nullptr;
	if (pHierarchyPoseGroup)
	{
		pHierarchy = pHierarchyPoseGroup->HierarchyPtr;
	}
	HTRHeader header;
	// TODO: get file name
	header.Name = filePath;

	for (int sectionIndex = 0; sectionIndex < sectionCount; ++sectionIndex)
	{
		FString section = sections[sectionIndex];

		TArray<FString> sectionNameFull;
		section.ParseIntoArray(sectionNameFull, _T("]"), true);
		FString sectionName = sectionNameFull[0];

		sectionName.ToUpperInline();

		if (sectionName == "HEADER")
		{
			LoadHTRHeader(header, section);
		}
		else if (sectionName == "SEGMENTNAMES&HIERARCHY")
		{
			if (pHierarchy) continue;
			LoadHTRHierarchy(pHierarchy, header, section);
		}
		else if (sectionName == "BASEPOSITION")
		{
			if (!pHierarchyPoseGroup)
			{
				LoadHTRBasePose(pHierarchyPoseGroup, pHierarchy, header, section);
			}
			// add space for new motions
			pHierarchyPoseGroup->AddNumUninitializedPoses(header.NumFrames);
		}
		else // Segment Motion e.g. [HEAD] or [LFOOT]
		{
			LoadHTRMotion(pHierarchyPoseGroup, header, section, loadMotionIntoBasePose);
		}
	}

	return header;
}

void HierarchyFileImporter::LoadHTRHeader(HTRHeader& outHeader, FString section)
{
	TArray<FString> lines;
	int32 lineCount = section.ParseIntoArrayLines(lines);
	outHeader.Initted = true;

	for (int i = 0; i < lines.Num(); ++i)
	{
		TArray<FString> words;
		lines[i].ParseIntoArrayWS(words);

		FString perameterName = words[0];
		FString perameterStr = words[1];
		if (perameterName == "FileType")
		{
			outHeader.FileType = perameterStr;
		}
		else if (perameterName == "DataType")
		{
			outHeader.DataType = perameterStr;
		}
		else if (perameterName == "FileVersion")
		{
			outHeader.FileVersion = FCString::Atoi(*perameterStr);

		}
		else if (perameterName == "NumSegments")
		{
			outHeader.NumSegments = FCString::Atoi(*perameterStr);
		}
		else if (perameterName == "NumFrames")
		{
			outHeader.NumFrames = FCString::Atoi(*perameterStr);
		}
		else if (perameterName == "DataFrameRate")
		{
			outHeader.DataFrameRate = FCString::Atoi(*perameterStr);

		}
		else if (perameterName == "EulerRotationOrder")
		{
			outHeader.EulerRotationOrder = perameterStr;
		}
		else if (perameterName == "CalibrationUnits")
		{
			outHeader.calibrationUnits = perameterStr;

		}
		else if (perameterName == "RotationUnits")
		{
			outHeader.RotationUnits = perameterStr;

		}
		else if (perameterName == "GlobalAxisofGravity")
		{
			outHeader.GlobalAxisofGravity = perameterStr[0];
		}
		else if (perameterName == "BoneLengthAxis")
		{
			outHeader.BoneLengthAxis = perameterStr[0];

		}
	}
}

void HierarchyFileImporter::LoadHTRHierarchy(Hierarchy*& outHierarchy, const HTRHeader& header, FString section)
{
	check(header.Initted);

	outHierarchy = new Hierarchy(header.NumSegments);

	TArray<FString> lines;
	int32 lineCount = section.ParseIntoArray(lines, _T("\r\n"), true);

	int32 nodeCount = 0;
	// start on first line, after section delimiter
	for (int32 i = 1; i < lines.Num(); i++)
	{
		TArray<FString> words;
		lines[i].ParseIntoArray(words, _T("\t"), true);
		// ignore comment lines
		if (words[0].Contains("#"))
		{
			continue;
		}

		FString nodeName = words[0].ToUpper();
		FString parentNodeName = words[1].ToUpper();
		// requires parent node be created first
		int32 parentNodeIndex = outHierarchy->GetNodeIndexFromName(parentNodeName);

		outHierarchy->SetNode(nodeCount++, parentNodeIndex, nodeName);
	}
}

void HierarchyFileImporter::LoadHTRBasePose(HierarchyPoseGroup*& outHierarchyPoseGroup, Hierarchy* hierarchy, const HTRHeader& header, FString section)
{
	check(header.Initted);

	outHierarchyPoseGroup = new HierarchyPoseGroup(hierarchy);

	TArray<FString> lines;
	int32 lineCount = section.ParseIntoArray(lines, _T("\r\n"), true);

	HierarchyPose* pBasePose = outHierarchyPoseGroup->GetBasePose();
	int32 j;
	SpatialPose* spatialPose = nullptr;
	for (int32 i = 1; i < lines.Num(); i++)
	{
		TArray<FString> words;
		lines[i].ParseIntoArray(words, _T("\t"), true);
		// ignore comment lines
		if (words[0].Contains("#"))
		{
			continue;
		}

		j = hierarchy->GetNodeIndexFromName(words[0]);
		spatialPose = pBasePose->Pose[j];

		FVector translation = FVector(FCString::Atof(*words[1]), FCString::Atof(*words[2]), FCString::Atof(*words[3]));

		spatialPose->SetTranslation(translation);

		FVector rotation = FVector(FCString::Atof(*words[4]), FCString::Atof(*words[5]), FCString::Atof(*words[6]));
		spatialPose->SetRotation(rotation);

		spatialPose->SetBoneLength(FCString::Atof(*words[7]));
	}
}

void HierarchyFileImporter::LoadHTRMotion(HierarchyPoseGroup* pHierarchyPoseGroup, const HTRHeader& header, FString section,
	bool loadMotionIntoBasePose)
{
	check(header.Initted);
	check(pHierarchyPoseGroup);

	// index of first pose in this file, starts at end of pose groups
	int32 firstPoseIndex = pHierarchyPoseGroup->HierarchicalPoses.Num() - header.NumFrames;

	TArray<FString> lines;
	int32 lineCount = section.ParseIntoArray(lines, _T("\r\n"), true);

	FString jointName = lines[0];
	jointName.RemoveFromEnd("]");

	SpatialPose* spatialPose = nullptr;
	for (int32 i = 1; i < lines.Num(); i++)
	{
		TArray<FString> words;
		lines[i].ParseIntoArray(words, _T("\t"), true);
		// ignore comment lines
		if (words[0].Contains("#"))
		{
			continue;
		}

		int32 p = firstPoseIndex + FCString::Atoi(*words[0]) - 1;
		int32 j = pHierarchyPoseGroup->HierarchyPtr->GetNodeIndexFromName(jointName);
		spatialPose = pHierarchyPoseGroup->GetSpatialPose(p, j);
		SpatialPose* baseSpatialPose = pHierarchyPoseGroup->mpBasePose->Pose[j];

		FVector translation = FVector(FCString::Atof(*words[1]), FCString::Atof(*words[2]), FCString::Atof(*words[3]));
		FVector rotation = FVector(FCString::Atof(*words[4]), FCString::Atof(*words[5]), FCString::Atof(*words[6]));
		const float uniformScale = FCString::Atof(*words[7]);
		FVector scale = FVector(uniformScale, uniformScale, uniformScale);

		if(loadMotionIntoBasePose)
		{
			baseSpatialPose->SetTranslation(translation);
			baseSpatialPose->SetRotation(rotation);
			baseSpatialPose->SetScale(scale);
			// set bone length of non-root nodes to magnitude of translation minus parent translation
			HierarchyNode* node = pHierarchyPoseGroup->HierarchyPtr->Nodes[j];
			if(node->ParentIndex >= 0)
			{
				baseSpatialPose->SetBoneLength(translation.Size());
			}
		}
		else
		{
			spatialPose->SetTranslation(translation - baseSpatialPose->Translation);
			spatialPose->SetRotation(rotation - baseSpatialPose->Orientation);
			spatialPose->SetScale(scale / baseSpatialPose->Scale);
		}
	}
}
