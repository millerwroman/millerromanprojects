// Fill out your copyright notice in the Description page of Project Settings.


#include "Keyframe.h"

Keyframe::Keyframe(float duration, int32 value)
{
	Duration = duration;
	DurationInv = 1.0f / duration;
	Value = value;
}

Keyframe::~Keyframe()
{
}
