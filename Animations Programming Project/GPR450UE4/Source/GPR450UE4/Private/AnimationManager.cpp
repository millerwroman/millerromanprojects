// Fill out your copyright notice in the Description page of Project Settings.


#include "AnimationManager.h"
#include "AnimationFileReader.h"
#include "ClipController.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

// Sets default values
AAnimationManager::AAnimationManager()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

// Called when the game starts or when spawned
void AAnimationManager::BeginPlay()
{
	Super::BeginPlay();

	AAnimationDataReader* AnimationReader = GetWorld()->SpawnActor<AAnimationDataReader>();
}

void AAnimationManager::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	ClipPool::Destroy();
	KeyframePool::Destroy();
}
