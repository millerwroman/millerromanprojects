// Fill out your copyright notice in the Description page of Project Settings.


#include "BlendNode.h"

BlendNode::BlendNode(HierarchyPose* (BlendNode::* exec)(),
	HierarchyPose* pose_out, TArray<HierarchyPose**> controlPoses, TArray<float*> params)
	: Exec(exec), mPose_out(pose_out), mControlPoses(controlPoses), mParams(params)
{
}

BlendNode::~BlendNode()
{
}

HierarchyPose* BlendNode::ExecStep()
{
	return HierarchyPose::Step(mPose_out, *mControlPoses[0]);
}

HierarchyPose* BlendNode::ExecNearest()
{
	return HierarchyPose::Nearest(mPose_out, *mControlPoses[0], *mControlPoses[1],*mParams[0]);
}

HierarchyPose* BlendNode::ExecSmoothStep()
{
	return HierarchyPose::Smootherstep(mPose_out, *mControlPoses[0], *mControlPoses[1], *mParams[0]);
}

HierarchyPose* BlendNode::ExecConvert()
{
	return HierarchyPose::Convert(mPose_out);
}

HierarchyPose* BlendNode::ExecConcat()
{
	return HierarchyPose::Concat(mPose_out, *mControlPoses[0], *mControlPoses[1]);
}

HierarchyPose* BlendNode::ExecLinear()
{
	return HierarchyPose::Linear(mPose_out, *mControlPoses[0], *mControlPoses[1], *mParams[0]);
}

HierarchyPose* BlendNode::ExecSmootherstep()
{
	return HierarchyPose::Smootherstep(mPose_out, *mControlPoses[0], *mControlPoses[1], *mParams[0]);
}

HierarchyPose* BlendNode::ExecCubic()
{
	return HierarchyPose::Cubic(mPose_out, *mControlPoses[0], *mControlPoses[1], *mControlPoses[2], *mControlPoses[3],
		*mParams[0]);
}

HierarchyPose* BlendNode::ExecTriangular()
{
	return HierarchyPose::Triangular(mPose_out, *mControlPoses[0], *mControlPoses[1], *mControlPoses[2],
		*mParams[0], *mParams[1]);
}

HierarchyPose* BlendNode::ExecBiNearest()
{
	return HierarchyPose::BiNearest(mPose_out, *mControlPoses[0], *mControlPoses[1], *mControlPoses[2], *mControlPoses[3],
		*mParams[0], *mParams[1], *mParams[2]);
}

HierarchyPose* BlendNode::ExecBiLinear()
{
	return HierarchyPose::BiLinear(mPose_out, *mControlPoses[0], *mControlPoses[1], *mControlPoses[2], *mControlPoses[3],
		*mParams[0], *mParams[1], *mParams[2]);
}

HierarchyPose* BlendNode::ExecBiCubic()
{
	return HierarchyPose::BiCubic(mPose_out,
		*mControlPoses[0], *mControlPoses[1], *mControlPoses[2], *mControlPoses[3],
		*mControlPoses[4], *mControlPoses[5], *mControlPoses[6], *mControlPoses[7],
		*mControlPoses[8], *mControlPoses[9], *mControlPoses[10], *mControlPoses[11],
		*mControlPoses[12], *mControlPoses[13], *mControlPoses[14], *mControlPoses[15],
		*mParams[0], *mParams[1], *mParams[2], *mParams[3], *mParams[4]);
}

BranchTransitionNode::BranchTransitionNode(HierarchyPose* pose_out,
	TArray<HierarchyPose**> controlPoses, TArray<float*> params,
	TFunction<void(FClipState& clipState, FKeyframeState& keyframeState)>* pTransition,
	bool (*branchCondition)(),
	FClipState* pClipState, FKeyframeState* PKeyframeState)
	:BlendNode((HierarchyPose*(BlendNode::*)())&BranchTransitionNode::Execute, pose_out, controlPoses, params),
	Transition(pTransition), BranchCondition(branchCondition),
	mpClipState(pClipState), mpKeyframeState(PKeyframeState)
{
}

BranchTransitionNode::~BranchTransitionNode()
{
}

HierarchyPose* BranchTransitionNode::Execute()
{
	bool newA = BranchCondition();
	// transition triggered
	if (newA != A)
	{
		A = newA;
		(*Transition)(*mpClipState, *mpKeyframeState);
		//Transition(*mClipController);
	}
	return A ? *mControlPoses[0] : *mControlPoses[1];
}


