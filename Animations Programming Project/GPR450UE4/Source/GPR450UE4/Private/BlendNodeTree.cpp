// Fill out your copyright notice in the Description page of Project Settings.


#include "BlendNodeTree.h"


BlendNodeTree::BlendNodeTree()
{
}

BlendNodeTree::~BlendNodeTree()
{
	for (int32 i = mBlendNodes.Num() - 1; i >= 0; --i)
	{
		delete mBlendNodes[i];
	}
	mBlendNodes.Empty();
}

int32 BlendNodeTree::AddNode(BlendNode* node)
{
	//mpHierarchy->SetNode()
	//int32 hierarchyIndex = mpHierarchy->GetNodeIndex(nodeName);
	//mBlendNodes[hierarchyIndex] = node;
	return mBlendNodes.Add(node);
}

void BlendNodeTree::ExecTree()
{
	// iterate through nodes
	//for (int32 i = mBlendNodes.Num() - 1; i >= 0; --i)
	for (int32 i = 0; i < mBlendNodes.Num(); ++i)
	{
		if (mBlendNodes[i])
		{
			(mBlendNodes[i]->*mBlendNodes[i]->Exec)();
		}
	}
}

