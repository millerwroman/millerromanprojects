//

#include "Clip.h"

Clip::Clip(FString& name, int firstKeyframeIndex, int lastKeyframeIndex, FClipTransition clipTransition,
	KeyframePool* keyframePool)
	: Name(name)
	, FirstKeyframeIndex(firstKeyframeIndex)
	, LastKeyframeIndex(lastKeyframeIndex)
	, ClipTransition(clipTransition)
	, mpKeyframePool(keyframePool)
{
	CalculateDuration();
}

Clip::~Clip()
{
}

void Clip::CalculateDuration()
{
	Duration = 0;
	for (int i = FirstKeyframeIndex; i <= LastKeyframeIndex; i++)
	{
		Duration += mpKeyframePool->GetKeyframe(i)->GetDuration();
	}
	DurationInv = 1.0f / Duration;
}

void Clip::DistributeDuration()
{
}
