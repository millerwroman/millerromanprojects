// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "KeyframePool.h"
#include "ClipTransition.h"

/**
 * Sequence of anim data.
 */
class Clip
{
public:
	Clip(FString& name, int firstKeyframeIndex, int lastKeyframeIndex, FClipTransition clipTransition,
		KeyframePool* keyframePool);
	~Clip();
public:
	float GetDuration() { return Duration; }
	float GetInverseDuration() { return DurationInv; }
	int32 GetFirstKeyframeIndex() { return FirstKeyframeIndex; }
	int32 GetLastKeyframeIndex() { return LastKeyframeIndex; }
	FString GetName() { return Name; }
	const FClipTransition& GetClipTransition() { return ClipTransition; }
private:
	void CalculateDuration(); // set both dur and durinv
	void DistributeDuration();
private:
	FString Name;
	float Duration; // duration of all keyframes in clip summed
	float DurationInv;

	//int32 KeyframeCount;
	int32 FirstKeyframeIndex;
	int32 LastKeyframeIndex;

	FClipTransition ClipTransition;
	KeyframePool* mpKeyframePool;
};
