// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * The most basic data point in animation
 */
class Keyframe
{

public:
	Keyframe(float duration, int32 value);
	~Keyframe();
public:
	float GetDuration() { return Duration; }
	float GetInverseDuration() { return DurationInv; }

	void SetValue(int32 value) { Value = value; }
	int32 GetValue() { return Value; }
private:
	float Duration;
	float DurationInv;

	int32 Value;
};
