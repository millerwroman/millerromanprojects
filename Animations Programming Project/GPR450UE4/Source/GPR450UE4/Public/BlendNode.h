// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ClipState.h"
#include "CoreMinimal.h"
#include "HierarchyPose.h"
#include "PoseFrame.h"
#include "SpatialPose.h"

/**
 * 
 */
class GPR450UE4_API BlendNode
{
public:
	BlendNode(HierarchyPose*(BlendNode::* exec)(), HierarchyPose* pose_out,
		TArray<HierarchyPose**> controlPoses, TArray<float*> params);
	~BlendNode();

	HierarchyPose* ExecStep();
	HierarchyPose* ExecNearest();
	HierarchyPose* ExecSmoothStep();
	HierarchyPose* ExecConvert();
	HierarchyPose* ExecConcat();
	HierarchyPose* ExecLinear();
	HierarchyPose* ExecSmootherstep();
	HierarchyPose* ExecCubic();
	HierarchyPose* ExecTriangular();
	HierarchyPose* ExecBiNearest();
	HierarchyPose* ExecBiLinear();
	HierarchyPose* ExecBiCubic();
	
public:
	HierarchyPose*(BlendNode::*Exec)();
	// variadic blendop broke passing float pointers
	//HierarchyPose* (*BlendOp)(HierarchyPose* p_out, ...);
	HierarchyPose* mPose_out;
	TArray<HierarchyPose**> mControlPoses;
	TArray<float*> mParams;
};

class BranchTransitionNode : public BlendNode
{
	BranchTransitionNode(HierarchyPose* pose_out,
		TArray<HierarchyPose**> controlPoses, TArray<float*> params,
		TFunction<void(FClipState& clipState, FKeyframeState& keyframeState)>* pTransition,
		bool (*branchCondition)(),
		FClipState* pClipState, FKeyframeState* PKeyframeState);
	~BranchTransitionNode();
	HierarchyPose* Execute();
	//HierarchyPose* (BlendNode::* Exec)();
	TFunction<void(FClipState& clipState, FKeyframeState& keyframeState)>* Transition;
	//void (*Transition)(FClipState&, FKeyframeState&);
	//void (*Transition)(ClipController*);
	bool (*BranchCondition)();

	bool A;
	FClipState* mpClipState;
	FKeyframeState* mpKeyframeState;
	//ClipController** mClipController;

};
