// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
UENUM()
enum class SpatialPoseChannel : uint32
{
	// identity (no channels)
	PoseChannel_none,

	// orientation
	PoseChannel_orient_x = 0x0001,
	PoseChannel_orient_y = 0x0002,
	PoseChannel_orient_z = 0x0004,
	PoseChannel_orient_xy = PoseChannel_orient_x | PoseChannel_orient_y,
	PoseChannel_orient_yz = PoseChannel_orient_y | PoseChannel_orient_z,
	PoseChannel_orient_zx = PoseChannel_orient_z | PoseChannel_orient_x,
	PoseChannel_orient_xyz = PoseChannel_orient_xy | PoseChannel_orient_z,

	// scale
	PoseChannel_scale_x = 0x0010,
	PoseChannel_scale_y = 0x0020,
	PoseChannel_scale_z = 0x0040,
	PoseChannel_scale_xy = PoseChannel_scale_x | PoseChannel_scale_y,
	PoseChannel_scale_yz = PoseChannel_scale_y | PoseChannel_scale_z,
	PoseChannel_scale_zx = PoseChannel_scale_z | PoseChannel_scale_x,
	PoseChannel_scale_xyz = PoseChannel_scale_xy | PoseChannel_scale_z,

	// translation
	PoseChannel_translate_x = 0x0100,
	PoseChannel_translate_y = 0x0200,
	PoseChannel_translate_z = 0x0400,
	PoseChannel_translate_xy = PoseChannel_translate_x | PoseChannel_translate_y,
	PoseChannel_translate_yz = PoseChannel_translate_y | PoseChannel_translate_z,
	PoseChannel_translate_zx = PoseChannel_translate_z | PoseChannel_translate_x,
	PoseChannel_translate_xyz = PoseChannel_translate_xy | PoseChannel_translate_z,

	PoseChannel_all = PoseChannel_orient_xyz | PoseChannel_scale_xyz | PoseChannel_translate_xyz
};
