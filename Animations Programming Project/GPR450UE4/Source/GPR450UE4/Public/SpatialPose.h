// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SpatialPoseEulerOrder.h"
#include "SpatialPoseChannel.h"
#include "Math/Vector.h"
#include "Math/TransformNonVectorized.h"
#include "GameFramework/Actor.h"

/**
 *
 */
class UWorld;

class GPR450UE4_API SpatialPose
{
public:
	SpatialPose();
	SpatialPose(const SpatialPose&);

public:
	~SpatialPose();

public:
	void SetRotation(const FVector orientation);
	void SetScale(const FVector scale);
	void SetTranslation(const FVector translation);
	void SetBoneLength(float boneLength);
public:
	// fundamental spatial pose / blend operations
	static SpatialPose Construct(FVector orientation = FVector(0, 0, 0),
		FVector translation = FVector(0, 0, 0),
		FVector scale = FVector(1, 1, 1));
	static SpatialPose Construct(FTransform transform = FTransform::Identity);
	static SpatialPose* Copy(SpatialPose* pose_out, const SpatialPose* p_rhs);
	// reset pose to identity
	static SpatialPose* ResetToIdentity(SpatialPose* p_out);
	static SpatialPose* InvertPiecewise(SpatialPose* p_out);
	static SpatialPose* InvertMatrix(SpatialPose* p_out);
	static SpatialPose* Concat(SpatialPose* p_out, const SpatialPose* p_lhs, const SpatialPose* p_rhs);
	static SpatialPose* Nearest(SpatialPose* p_out, const SpatialPose* p0, const SpatialPose* p1, float u);
	static SpatialPose* Lerp(SpatialPose* p_out, const SpatialPose* p0, const SpatialPose* p1, float u);
	static SpatialPose* Smoothstep(SpatialPose* p_out, const SpatialPose* p0, const SpatialPose* p1, float u);
	static SpatialPose* Smootherstep(SpatialPose* p_out, const SpatialPose* p0, const SpatialPose* p1, float u);
	// catmull-rom spline cubic
	static SpatialPose* Cubic(SpatialPose* p_out, const SpatialPose* pP, const SpatialPose* p0, const SpatialPose* p1, const SpatialPose* pN, float u);
	// converts raw components into homogenous transform mat
	static SpatialPose* Convert(SpatialPose* p_out);
	// reverts homogenous transform mat into components
	static SpatialPose* Revert(SpatialPose* p_out);
	
	// derivative blend operations
	static SpatialPose* Deconcat(SpatialPose* p_out, const SpatialPose* p_lhs, const SpatialPose* p_rhs);
	static SpatialPose* ScalePose(SpatialPose* p_out, float scalar);
	// descale
	static SpatialPose* Divide(SpatialPose* p_out, float divisor);
	static SpatialPose* Triangular(SpatialPose* p_out, SpatialPose* p0, SpatialPose* p1, SpatialPose* p2, float u1, float u2);
	static SpatialPose* BiNearest(SpatialPose* p_out, SpatialPose* p0, SpatialPose* p1, SpatialPose* p2, SpatialPose* p3, float u0, float u1, float u);
	static SpatialPose* BiLinear(SpatialPose* p_out, SpatialPose* p0, SpatialPose* p1, SpatialPose* p2, SpatialPose* p3, float u0, float u1, float u);
	static SpatialPose* BiCubic(SpatialPose* p_out,
		SpatialPose* p0, SpatialPose* p1, SpatialPose* p2, SpatialPose* p3,
		SpatialPose* p4, SpatialPose* p5, SpatialPose* p6, SpatialPose* p7,
		SpatialPose* p8, SpatialPose* p9, SpatialPose* p10, SpatialPose* p11,
		SpatialPose* p12, SpatialPose* p13, SpatialPose* p14, SpatialPose* p15,
		float u0, float u1, float u2, float u3, float u);

	// solves for object space transform
	static SpatialPose* ForwardKinematics(SpatialPose* p_obj_out, SpatialPose* p_local, SpatialPose* p_parent_obj);
	static SpatialPose* InverseKinematics(SpatialPose* p_local_out, SpatialPose* p_obj, SpatialPose* p_parent_obj);
	
public:
	FTransform Transform;
	
	FVector Orientation;
	FVector Scale;
	FVector Translation;
	float BoneLength;
};
