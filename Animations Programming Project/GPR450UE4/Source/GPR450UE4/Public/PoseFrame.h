// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HierarchyPoseGroup.h"
#include "KeyframeState.h"

/**
 * 
 */
class GPR450UE4_API PoseFrame
{
public:
	PoseFrame(HierarchyPoseGroup* pHierarchyPoseGroup, FKeyframeState* pKeyframeState)
		: mpHierarchyPoseGroup(pHierarchyPoseGroup), mpKeyframeState(pKeyframeState)
	{}
	~PoseFrame() {};

	void Update();

public:
	HierarchyPose* pP;
	HierarchyPose* p0;
	HierarchyPose* p1;
	HierarchyPose* pN;

private:
	HierarchyPoseGroup* mpHierarchyPoseGroup;
	FKeyframeState* mpKeyframeState;
};
