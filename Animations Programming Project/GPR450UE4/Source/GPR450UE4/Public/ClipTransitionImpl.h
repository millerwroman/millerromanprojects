#pragma once

#include "CoreMinimal.h"
#include "ClipState.h"
#include "KeyframeState.h"

class ClipTransitionImpl
{
private:
	static TMap<ETransitionAction,
		TFunction<void(FClipState& clipState, FKeyframeState& keyframeState)>> Transitions;
public:
	static void Transition(FClipState& clipState, FKeyframeState& keyframeState);
	// Update keyframe state with a new keyframe
	static void SetKeyframe(const FClipState& clipState, FKeyframeState& keyframeState,
		int32 keyframeIndex);
private:
	static void SetClip(FClipState& clipState, const FString& targetClipName);
	// Pause at current terminus of clip (start of first frame or end of last frame)
	static void PauseTransition(FClipState& clipState, FKeyframeState& keyframeState);
	// Forward playback from start of clip (true clip start; includes playback of first frame)
	static void ForwardTransition(FClipState& clipState, FKeyframeState& keyframeState);
	// Forward pause at start of first frame
	static void ForwardPauseTransition(FClipState& clipState, FKeyframeState& keyframeState);
	// Reverse playback from end of clip(true clip end; includes playback of last frame)
	static void ReverseTransition(FClipState& clipState, FKeyframeState& keyframeState);
	// Reverse pause at end of last frame
	static void ReversePauseTransition(FClipState& clipState, FKeyframeState& keyframeState);
	// Forward playback from end of first frame (skip first; excludes playback of first frame)
	static void ForwardSkipTransition(FClipState& clipState, FKeyframeState& keyframeState);
	// Forward pause at end of first frame
	static void ForwardSkipPauseTransition(FClipState& clipState, FKeyframeState& keyframeState);
	// Reverse playback from start of last frame (skip last; excludes playback of last frame)
	static void ReverseSkipTransition(FClipState& clipState, FKeyframeState& keyframeState);
	// Reverse pause at start of last frame
	static void ReverseSkipPauseTransition(FClipState& clipState, FKeyframeState& keyframeState);
};
