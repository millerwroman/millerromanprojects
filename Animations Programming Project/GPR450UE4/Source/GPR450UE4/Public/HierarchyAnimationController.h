// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HierarchyState.h"
#include "ClipController.h"
#include "PoseInterpolationMode.h"
#include "BlendNodeTree.h"
#include "PoseFrame.h"
#include "LocomotionMode.h"
#include "LocationRotation.h"

#include "HierarchyAnimationController.generated.h"


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class GPR450UE4_API UHierarchyAnimationController : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UHierarchyAnimationController();

protected:
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

public:
	UFUNCTION(BlueprintCallable)
	void SetInputLinearX(float axis) { mMoveInput.Linear.X = axis; }
	UFUNCTION(BlueprintCallable)
	void SetInputLinearY(float axis) { mMoveInput.Linear.Y = axis; }
	UFUNCTION(BlueprintCallable)
	void SetInputAngularZ(float axis) { mMoveInput.Angular.Z = axis; }
	UFUNCTION(BlueprintCallable)
	void SetIsJumping(bool value) { mIsJumping = value; }
	UFUNCTION(BlueprintCallable)
	void SetLocomotionModeLinear(ELocomotionMode mode) { LocomotionModeLinear = mode; }
	UFUNCTION(BlueprintCallable)
	void SetLocomotionModeAngular(ELocomotionMode mode) { LocomotionModeAngular = mode; }

private:
	void InitHierarchy();
	void CleanupHierarchy();
	// sets leaf node references to hierarchy state & pose group poses
	void InitBlendTree();
	void CleanupBlendTree();
	void LocomotionLinear(FVector inputLinear, float dt);
	void LocomotionAngular(FVector inputAngular, float dt);
	static FVector IntegrateEuler(const FVector& x, const FVector& dx_dt, float dt);
	static FVector IntegrateKinematic(const FVector& x, const FVector& dx_dt, const FVector& dx_dt2, float dt);
	static FVector IntegrateLerp(const FVector& x, const FVector& x_f, float u);

	void NeckIK();
	void LeftArmIK();
	// generalized implementation of unconstrained IK
	void UnconstrainedIK(int32 endJointIndex, int32 baseJointIndex, SpatialPose& endEffectorSpatialPose, FVector(*getChainRotDir)(FVector chainDir), int32 maxLoops, float dt);
	void WanderEndEffectorTranslation(SpatialPose& endEffectorTargetTranslation, FVector& wanderTarget, FVector wanderCenter, float wanderRadius, float wanderTolerance, float wanderMaxVel, FVector& wanderVel, float dt);

private:
	UPROPERTY(EditAnywhere)
	AActor* LookTarget;
	UPROPERTY(EditAnywhere)
	AActor* LeftHandEndEffectorActor;
	UPROPERTY(EditAnywhere)
	USceneComponent* LeftArmPlaneConstraintTransform;

	UPROPERTY(EditAnywhere)
	int32 MaxSpineLoops;
	UPROPERTY(EditAnywhere)
	float SpineTolerance;
	UPROPERTY(EditAnywhere)
	float WanderTolerance;
	UPROPERTY(EditAnywhere)
	float NeckWanderRadius;
	UPROPERTY(EditAnywhere)
	float NeckWanderMaxVel;
	UPROPERTY(EditAnywhere)
	float ArmWanderRadius;
	UPROPERTY(EditAnywhere)
	float ArmWanderMaxVel;
	
	
	UPROPERTY(EditAnywhere)
	UClass* JointActorClass;
	UPROPERTY(EditAnywhere)
	ELocomotionMode LocomotionModeLinear;
	UPROPERTY(EditAnywhere)
	ELocomotionMode LocomotionModeAngular;
	UPROPERTY(EditAnywhere)
	float LocomotionEulerVelocityLinear = 1.0f;
	UPROPERTY(EditAnywhere)
	float LocomotionEulerVelocityAngular = 1.0f;
	UPROPERTY(EditAnywhere)
	float LocomotionKinematicAccelLinear = 1.0f;
	UPROPERTY(EditAnywhere)
	float LocomotionKinematicAccelAngular = 1.0f;
	UPROPERTY(EditAnywhere)
	float LocomotionLerpPosLinearRadius = 300.0f;
	UPROPERTY(EditAnywhere)
	float LocomotionLerpPosAngularRadius = 179.0f;
	UPROPERTY(EditAnywhere)
	float LocomotionLerpVelLinearRadius = 300.0f;
	UPROPERTY(EditAnywhere)
	float LocomotionLerpVelAngularRadius = 179.0f;
	UPROPERTY(EditAnywhere)
	EPoseInterpolationMode PoseInterpolationMode;
	UPROPERTY(EditAnywhere)
	FString BasePoseHtrPath;
	UPROPERTY(EditAnywhere)
	TArray<FString> HtrPaths;
	UPROPERTY(EditAnywhere)
	int idleClipIndex = 0;
	UPROPERTY(EditAnywhere)
	int walkClipIndex = 1;
	UPROPERTY(EditAnywhere)
	int runClipIndex = 2;
	UPROPERTY(EditAnywhere)
	int jumpClipIndex = 3;

private:
	HierarchyPoseGroup* mpHierarchyPoseGroup;
	HierarchyState* mpHierarchyState;

	TArray<ClipController> mClipControllers;
	TArray<PoseFrame> mPoseFrames;
	
	BlendNodeTree* mpBlendNodeTree;
	// rendered joint actors
	TArray<AActor*> mJointActors;

	LocationRotation mOrigin;
	
	LocationRotation mVelocity;
	float mVelocityLinearPercent;
	float mVelocityAngularPercent;
	LocationRotation mAccel;

	LocationRotation mMoveInput;
	float mIsJumping;

	HierarchyPose* mpIdleSmoothstepPose;
	HierarchyPose* mpWalkSmoothstepPose;
	HierarchyPose* mpRunSmoothstepPose;
	HierarchyPose* mpIdleRunLerpPose;
	HierarchyPose* mpJumpSmoothstepPose;


	SpatialPose neckEndEffectorSpatialPose;
	FVector neckWanderTarget;
	FVector neckWanderVel;

	SpatialPose leftArmEndEffectorSpatialPose;
	FVector leftArmWanderTarget;
	FVector leftArmWanderVel;

	SpatialPose rightArmEndEffectorSpatialPose;
	FVector rightArmWanderTarget;
	FVector rightArmWanderVel;
	
	bool firstWander;
};
