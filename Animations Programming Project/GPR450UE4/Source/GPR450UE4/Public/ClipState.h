//

#pragma once

#include "CoreMinimal.h"
#include "Clip.h"
#include "PlaybackMode.h"

#include "ClipState.generated.h"

USTRUCT()
struct FClipState
{
	GENERATED_BODY()

public:
	int32 CurClipIndex;
	Clip* CurClip;
	float ClipDuration;  // store duration and inv for quick access
	float ClipDurationInv;
	float ClipTime;
	float ClipParameter;
	EPlaybackMode PlaybackMode;
};
