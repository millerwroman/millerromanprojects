// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "HierarchyPoseGroup.h"
#include "CoreMinimal.h"

struct HTRHeader;

/**
 *
 */
class GPR450UE4_API HierarchyFileImporter
{
public:
	// append htr motions onto hierarchy pose group
	static HTRHeader LoadHTR(FString filePath, HierarchyPoseGroup*& pHierarchyPoseGroup,
		bool loadMotionIntoBasePose = false);

private:
	static void LoadHTRHeader(HTRHeader& outHeader, FString section);
	static void LoadHTRHierarchy(Hierarchy*& outHierarchy, const HTRHeader& header, FString section);
	static void LoadHTRBasePose(HierarchyPoseGroup*& outHierarchyPoseGroup, Hierarchy* hierarchy, const HTRHeader& header, FString section);
	static void LoadHTRMotion(HierarchyPoseGroup* pHierarchyPoseGroup, const HTRHeader& header, FString section,
		bool loadMotionIntoBasePose = false);

};

struct HTRHeader
{
	FString Name;
	FString FileType, DataType, EulerRotationOrder, calibrationUnits, RotationUnits;
	TCHAR GlobalAxisofGravity, BoneLengthAxis; //X, Y, Z
	int32 FileVersion, NumSegments, NumFrames, DataFrameRate;
	bool Initted = false;
};
