// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HierarchyPoseGroup.h"
#include "KeyframeState.h"
#include "PoseInterpolationMode.h"

/*
 * aggregate all of the information about a hierarchical pose and convert it to a format
 * that can be used by other systems, such as graphics, at any time
 */
class GPR450UE4_API HierarchyState
{
public:
	HierarchyState(const HierarchyPoseGroup* hierarchyPoseGroup);
	~HierarchyState();
public:
	// update inverse object-space matrices
	void UpdateObjectInverse();

	void Update(FKeyframeState& keyframeState, EPoseInterpolationMode poseInterpMode);

public:
	// A pointer or reference to the hierarchy associated with this spatial state.
	const HierarchyPoseGroup* pHierarchyPoseGroup;
	// A hierarchical pose representing each node's animated pose at the current time.
	HierarchyPose* DeltaPose;
	// A hierarchical pose representing each node's transformation relative to its parent's space.
	HierarchyPose* LocalSpacePose;
	// A hierarchical pose representing each node's transformation relative to the root's parent space (the actual object that the hierarchy represents).
	// [0] is base pose
	HierarchyPose* ObjectSpacePose;
	HierarchyPose* WorldSpacePose;

	int32 NumNodes;

	int32 CurPoseIndex;
};
