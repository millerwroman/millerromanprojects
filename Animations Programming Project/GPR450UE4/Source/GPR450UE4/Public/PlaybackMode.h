//

#pragma once

#include "CoreMinimal.h"

UENUM(BlueprintType)
enum class EPlaybackMode : uint8
{
	Pause,
	Forward,
	Reverse
};
