//

#pragma once

#include "CoreMinimal.h"

#include "ClipTransition.generated.h"

UENUM()
enum class ETransitionAction : uint32
{
	Pause,            // Pause at current terminus of clip (start of first frame or end of last frame)
	Forward,          // Forward playback from start of clip (true clip start; includes playback of first frame)
	ForwardPause,     // Forward pause at start of first frame
	Reverse,          // Reverse playback from end of clip (true clip end; includes playback of last frame)
	ReversePause,     // Reverse pause at end of last frame
	ForwardSkip,      // Forward playback from end of first frame (skip first; excludes playback of first frame)
	ForwardSkipPause, // Forward pause at end of first frame
	ReverseSkip,      // Reverse playback from start of last frame(skip last; excludes playback of last frame)
	ReverseSkipPause  // Reverse pause at start of last frame
};

USTRUCT()
struct FClipTransition
{
	GENERATED_BODY()
public:
	FClipTransition() {}
	FClipTransition(ETransitionAction transitionAction, FString targetClipName)
	{
		mTransitionAction = transitionAction;
		mTargetClipName = targetClipName;
	}
	ETransitionAction mTransitionAction;
	FString mTargetClipName;
};
