//

#pragma once

#include "CoreMinimal.h"

// list of Euler angle product orders
UENUM()
enum class SpatialPoseEulerOrder : uint8
{
	PoseEulerOrder_xyz,
	PoseEulerOrder_yzx,
	PoseEulerOrder_zxy,
	PoseEulerOrder_yxz,
	PoseEulerOrder_xzy,
	PoseEulerOrder_zyx,
};
