// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Clip.h"
#include "KeyframePool.h"


/**
 * 
 */
class ClipPool
{
public:
	// lazy get instance
	static ClipPool* GetInstance();
	static void Destroy() { delete ClipPool::GetInstance(); }

	Clip* GetClip(int32 index);
	int32 AddClip(Clip* clip);

	int32 GetClipIndex(FString name);
	int32 GetNumClips() { return Clips.Num(); }
private:
	ClipPool();
	~ClipPool();
private:
	TArray<Clip*> Clips;

	// singleton
	static ClipPool* instance;
};
