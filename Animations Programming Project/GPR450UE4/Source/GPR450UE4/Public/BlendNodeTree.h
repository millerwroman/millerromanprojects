// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Hierarchy.h"
#include "BlendNode.h"

/**
 * 
 */
class GPR450UE4_API BlendNodeTree
{
public:
	BlendNodeTree();
	~BlendNodeTree();

	// add root first, leaves last
	int32 AddNode(BlendNode* node);
	void ExecTree();
public:
	TArray<BlendNode*> mBlendNodes;
};
