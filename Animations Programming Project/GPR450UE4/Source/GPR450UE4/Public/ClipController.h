/*
*	ClipController: The playhead of Animation
*	Contributors: Adam Clarke, Miller Roman
*/

#pragma once

#include "CoreMinimal.h"
#include "KeyframePool.h"
#include "ClipPool.h"
#include "KeyframeState.h"
#include "ClipState.h"
#include "ClipTransition.h"
#include "ClipTransitionImpl.h"

class GPR450UE4_API ClipController
{
public:
	ClipController();
	~ClipController();
public:
	// Initialize clip controller
	void Init(KeyframePool* keyframePool, ClipPool* clipPool, uint32 initialClipIndex);

	// converts realtime to animation time and resolves keyframe
	FKeyframeState& Update(float DeltaTime);

	FString GetName() { return ControllerName; }
	float GetSpeed() { return Speed; }
	// adjust speed multiplier
	void SetSpeed(float speed);
	// play, reverse, pause
	void SetPlaybackMode(EPlaybackMode mode)
	{
		//UE_LOG(LogTemp, Warning, TEXT("SETPLAYBACKMODE: %i"), mode);
		mClipState.PlaybackMode = mode;
	}

	// reset time to 0 and keyframe to first
	void ResetToFirstKeyframe();
	// reset time to duration and keyframe to last
	void ResetToLastKeyframe();
	// change clip
	void SetClip(int32 clipIndex);

	FString GetClipControllerName() { return ControllerName; }
private:
	// resolve time by checking forward cases
	void ResolveForward();
	// resolve time by checking reverse cases
	void ResolveReverse();
private:
	FString ControllerName = "Controller";
	// speed multiplier for animation time
	float Speed = 1.0f;
private:
	KeyframePool* mpKeyframePool;
	ClipPool* mpClipPool;
public:
	FClipState mClipState;
	FKeyframeState mKeyframeState;
};
