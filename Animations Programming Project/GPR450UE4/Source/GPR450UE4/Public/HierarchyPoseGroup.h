// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Hierarchy.h"
#include "HierarchyPose.h"

/**
 * 
 */
class GPR450UE4_API HierarchyPoseGroup
{
public:
	HierarchyPoseGroup(Hierarchy* hierarchy);
	~HierarchyPoseGroup();
public:

	// get offset to hierarchy pose in contiguous set
	int32 GetPoseOffsetIndex(const uint32 poseIndex);

	// get offset to single node pose in contiguous set
	int32 GetNodePoseOffsetIndex(const uint32 poseIndex, const uint32 nodeIndex);

	SpatialPose* GetSpatialPose(int32 poseIndex, int32 nodeIndex);
	HierarchyPose* GetBasePose() const { return mpBasePose; }
	// add uninitialized length to hierarchichal poses
	void AddNumUninitializedPoses(int32 num);

public:
	// the base heirarchy / skeleton of this animation
	Hierarchy* HierarchyPtr;
	// base pose
	HierarchyPose* mpBasePose;
	// organizes above spatial pose pools into an array of actual keyframe poses
	TArray<HierarchyPose*> HierarchicalPoses;
};
