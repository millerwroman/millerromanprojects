//// Fill out your copyright notice in the Description page of Project Settings.
//
//#pragma once
//
//#include "CoreMinimal.h"
//
//class HierarchyState;
//
///**
// * 
// */
//class GPR450UE4_API Kinematics
//{
//public:
//	// forward kinematics solver given an initialized hierarchy state
//	static void KinematicsSolveForward(const HierarchyState* hierarchyState);
//
//	// forward kinematics solver starting at a specified joint
//	static void KinematicsSolveForwardPartial(const HierarchyState* hierarchyState, const uint32 firstIndex, const uint32 nodeCount);
//
//	// inverse kinematics solver given an initialized hierarchy state
//	static void KinematicsSolveInverse(const HierarchyState* hierarchyState);
//
//	// inverse kinematics solver starting at a specified joint
//	static void KinematicsSolveInversePartial(const HierarchyState* hierarchyState, const uint32 firstIndex, const uint32 nodeCount);
//};
