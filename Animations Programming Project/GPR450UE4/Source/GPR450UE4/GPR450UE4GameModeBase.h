// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GPR450UE4GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class GPR450UE4_API AGPR450UE4GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
